/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package entities;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Sana
 */
@Entity
@Table(name = "NEWS_POINTER")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "NewsPointer.findAll", query = "SELECT n FROM NewsPointer n"),
    @NamedQuery(name = "NewsPointer.findByPointerId", query = "SELECT n FROM NewsPointer n WHERE n.pointerId = :pointerId")})
public class NewsPointer implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "POINTER_ID")
    private Integer pointerId;
    @JoinColumn(name = "USER_ID", referencedColumnName = "USER_ID")
    @OneToOne(optional = false)
    private NetworkUsers userId;
    @JoinColumn(name = "NEWS_ID", referencedColumnName = "NEWS_ID")
    @ManyToOne(optional = false)
    private News newsId;

    public NewsPointer() {
    }

    public NewsPointer(Integer pointerId) {
        this.pointerId = pointerId;
    }

    public Integer getPointerId() {
        return pointerId;
    }

    public void setPointerId(Integer pointerId) {
        this.pointerId = pointerId;
    }

    public NetworkUsers getUserId() {
        return userId;
    }

    public void setUserId(NetworkUsers userId) {
        this.userId = userId;
    }

    public News getNewsId() {
        return newsId;
    }

    public void setNewsId(News newsId) {
        this.newsId = newsId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (pointerId != null ? pointerId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof NewsPointer)) {
            return false;
        }
        NewsPointer other = (NewsPointer) object;
        if ((this.pointerId == null && other.pointerId != null) || (this.pointerId != null && !this.pointerId.equals(other.pointerId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entities.NewsPointer[ pointerId=" + pointerId + " ]";
    }
    
}
