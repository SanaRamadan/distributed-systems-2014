/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package entities;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Sana
 */
@Entity
@Table(name = "NETWORK_GROUPS")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "NetworkGroups.findAll", query = "SELECT n FROM NetworkGroups n"),
    @NamedQuery(name = "NetworkGroups.findByGroupName", query = "SELECT n FROM NetworkGroups n WHERE n.groupName = :groupName"),
    @NamedQuery(name = "NetworkGroups.findByGroupId", query = "SELECT n FROM NetworkGroups n WHERE n.groupId = :groupId")})
public class NetworkGroups implements Serializable {
    private static final long serialVersionUID = 1L;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 32)
    @Column(name = "GROUP_NAME")
    private String groupName;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "GROUP_ID")
    private Integer groupId;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "groupId")
    private Collection<GroupPermissions> groupPermissionsCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "groupId")
    private Collection<News> newsCollection;

    public NetworkGroups() {
    }

    public NetworkGroups(Integer groupId) {
        this.groupId = groupId;
    }

    public NetworkGroups(Integer groupId, String groupName) {
        this.groupId = groupId;
        this.groupName = groupName;
    }

    public String getGroupName() {
        return groupName;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }

    public Integer getGroupId() {
        return groupId;
    }

    public void setGroupId(Integer groupId) {
        this.groupId = groupId;
    }

    @XmlTransient
    public Collection<GroupPermissions> getGroupPermissionsCollection() {
        return groupPermissionsCollection;
    }

    public void setGroupPermissionsCollection(Collection<GroupPermissions> groupPermissionsCollection) {
        this.groupPermissionsCollection = groupPermissionsCollection;
    }

    @XmlTransient
    public Collection<News> getNewsCollection() {
        return newsCollection;
    }

    public void setNewsCollection(Collection<News> newsCollection) {
        this.newsCollection = newsCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (groupId != null ? groupId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof NetworkGroups)) {
            return false;
        }
        NetworkGroups other = (NetworkGroups) object;
        if ((this.groupId == null && other.groupId != null) || (this.groupId != null && !this.groupId.equals(other.groupId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entities.NetworkGroups[ groupId=" + groupId + " ]";
    }
    
}
