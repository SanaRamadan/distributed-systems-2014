/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package entities;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Sana
 */
@Entity
@Table(name = "NETWORK_USERS")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "NetworkUsers.findAll", query = "SELECT n FROM NetworkUsers n"),
    @NamedQuery(name = "NetworkUsers.findByUserName", query = "SELECT n FROM NetworkUsers n WHERE n.userName = :userName"),
    @NamedQuery(name = "NetworkUsers.findByUserId", query = "SELECT n FROM NetworkUsers n WHERE n.userId = :userId"),
    @NamedQuery(name = "NetworkUsers.findByEmail", query = "SELECT n FROM NetworkUsers n WHERE n.email = :email"),
    @NamedQuery(name = "NetworkUsers.findByPassword", query = "SELECT n FROM NetworkUsers n WHERE n.password = :password")})
public class NetworkUsers implements Serializable {
    private static final long serialVersionUID = 1L;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 32)
    @Column(name = "USER_NAME")
    private String userName;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "USER_ID")
    private Integer userId;
    // @Pattern(regexp="[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?", message="Invalid email")//if the field contains email address consider using this annotation to enforce field validation
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 64)
    @Column(name = "EMAIL")
    private String email;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 16)
    @Column(name = "PASSWORD")
    private String password;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "userId")
    private Collection<GroupPermissions> groupPermissionsCollection;
    @OneToOne(cascade = CascadeType.ALL, mappedBy = "userId")
    private NewsPointer newsPointer;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "userId")
    private Collection<News> newsCollection;

    public NetworkUsers() {
    }

    public NetworkUsers(Integer userId) {
        this.userId = userId;
    }

    public NetworkUsers(Integer userId, String userName, String email, String password) {
        this.userId = userId;
        this.userName = userName;
        this.email = email;
        this.password = password;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @XmlTransient
    public Collection<GroupPermissions> getGroupPermissionsCollection() {
        return groupPermissionsCollection;
    }

    public void setGroupPermissionsCollection(Collection<GroupPermissions> groupPermissionsCollection) {
        this.groupPermissionsCollection = groupPermissionsCollection;
    }

    public NewsPointer getNewsPointer() {
        return newsPointer;
    }

    public void setNewsPointer(NewsPointer newsPointer) {
        this.newsPointer = newsPointer;
    }

    @XmlTransient
    public Collection<News> getNewsCollection() {
        return newsCollection;
    }

    public void setNewsCollection(Collection<News> newsCollection) {
        this.newsCollection = newsCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (userId != null ? userId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof NetworkUsers)) {
            return false;
        }
        NetworkUsers other = (NetworkUsers) object;
        if ((this.userId == null && other.userId != null) || (this.userId != null && !this.userId.equals(other.userId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entities.NetworkUsers[ userId=" + userId + " ]";
    }
    
}
