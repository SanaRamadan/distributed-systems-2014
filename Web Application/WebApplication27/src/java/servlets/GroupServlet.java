package servlets;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import entities.NetworkGroups;
import entities.News;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import sessions.NetworkGroupsSessionLocal;
import sessions.NewsSessionLocal;

/**
 *
 * @author Sana
 */
@WebServlet(name = "Group", urlPatterns = {"/Group"})
public class GroupServlet extends HttpServlet {

    @EJB
    private NewsSessionLocal newsSession;
    @EJB
    private NetworkGroupsSessionLocal groupSession;

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        int groupId = Integer.parseInt(request.getParameter("groupId"));

        GROUPCommand command = new GROUPCommand(groupId);
        command.execute();
        request.setAttribute("GroupInfo", command);
        request.getRequestDispatcher("Group.jsp").forward(request, response);
    }

    public GROUPCommand group(int groupId) {
        return null;
    }

    public class GROUPCommand {

        public int groupId;
        public String groupName;
        public int newsCount;
        public int firstNews;
        public int lastNews;

        public GROUPCommand(int groupId) {
            this.groupId = groupId;
            newsCount = 0;
        }

        public GROUPCommand execute() {

            List<News> allNews = newsSession.findAll();
            List<NetworkGroups> allGroups = groupSession.findAll();

            for (NetworkGroups networkGroup : allGroups) {
                if (networkGroup.getGroupId() == groupId) {
                    groupName = networkGroup.getGroupName();
                }
            }
            List<Integer> newsForGroup = new ArrayList<Integer>();
            for (News news : allNews) {
                if (news.getGroupId().getGroupId() == groupId) {
                    newsCount++;
                    newsForGroup.add(news.getNewsId());
                }
            }
            int max = 1, min = newsForGroup.size() - 1;
            for (int i = 0; i < newsForGroup.size(); i++) {
                if (newsForGroup.get(i) > max) {
                    max = newsForGroup.get(i);
                }
                if (newsForGroup.get(i) < min) {
                    min = newsForGroup.get(i);
                }

            }
            firstNews = min;
            lastNews = max;

            return this;
        }

        public int getGroupId() {
            return groupId;
        }

        public void setGroupId(int groupId) {
            this.groupId = groupId;
        }

        public String getGroupName() {
            return groupName;
        }

        public void setGroupName(String groupName) {
            this.groupName = groupName;
        }

        public int getNewsCount() {
            return newsCount;
        }

        public void setNewsCount(int newsCount) {
            this.newsCount = newsCount;
        }

        public int getFirstNews() {
            return firstNews;
        }

        public void setFirstNews(int firstNews) {
            this.firstNews = firstNews;
        }

        public int getLastNews() {
            return lastNews;
        }

        public void setLastNews(int lastNews) {
            this.lastNews = lastNews;
        }

    }
    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
