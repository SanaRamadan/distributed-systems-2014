/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sessions;

import entities.News;
import java.util.List;
import javax.ejb.LocalBean;
import javax.persistence.EntityManager;

/**
 *
 * @author Sana
 */
@LocalBean
public interface NewsSessionLocal {

    public String createe();

    abstract EntityManager getEntityManager();

    public void create(News entity);

    public void edit(News entity);

    public void remove(News entity);

    public News find(int id);

    public List<News> findAll();

    public int count(int groupId);
}
