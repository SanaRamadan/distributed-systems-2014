/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sessions;

import entities.NetworkGroups;
import entities.News;
import java.util.List;
import javax.ejb.LocalBean;
import javax.persistence.EntityManager;

/**
 *
 * @author Sana
 */
@LocalBean
public interface NetworkGroupsSessionLocal {

    abstract EntityManager getEntityManager();

    public void create(NetworkGroups entity);

    public List<NetworkGroups> findAll();

}
