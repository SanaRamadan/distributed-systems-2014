/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sessions;

import entities.NetworkGroups;
import entities.News;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author Sana
 */
@Stateless
public class NetworkGroupsSession implements NetworkGroupsSessionLocal {

    @PersistenceContext 
    private EntityManager em;

    @Override
    public void create(NetworkGroups entity) {
        getEntityManager().persist(entity);
    }

    @Override
    public List<NetworkGroups> findAll() {
        javax.persistence.criteria.CriteriaQuery cq = getEntityManager().getCriteriaBuilder().createQuery();
        cq.select(cq.from(NetworkGroups.class));
        return getEntityManager().createQuery(cq).getResultList();
    }

    @Override
    public EntityManager getEntityManager() {
        return em;
    }

}
