/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sessions;

import entities.GroupPermissions;
import entities.News;
import javax.ejb.LocalBean;
import javax.persistence.EntityManager;

/**
 *
 * @author Sana
 */
@LocalBean
public interface GroupPermissionSessionLocal {
 
    abstract EntityManager getEntityManager();

    public void create(GroupPermissions entity); 

    public GroupPermissions find(int id); 
}
