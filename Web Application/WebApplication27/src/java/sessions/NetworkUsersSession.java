/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sessions;

import entities.NetworkUsers;
import entities.News;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author Sana
 */
@Stateless
public class NetworkUsersSession implements NetworkUsersSessionLocal {

    @PersistenceContext
    private EntityManager em;

    @Override
    public void create(NetworkUsers entity) {
        getEntityManager().persist(entity);
    }

    @Override
    public EntityManager getEntityManager() {
        return em;
    }

}
