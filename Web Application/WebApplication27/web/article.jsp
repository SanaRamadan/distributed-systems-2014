<%-- 
    Document   : blog
    Created on : May 20, 2014, 1:28:01 PM
    Author     : Bassam
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<style type="text/css">
.auto-style1 {
	margin-right: 297px;
}
</style>
<!--[if IE 7 ]>    <html class="ie7 oldie"> <![endif]-->
<!--[if IE 8 ]>    <html class="ie8 oldie"> <![endif]-->
<!--[if IE 9 ]>    <html class="ie9"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--> <html> <!--<![endif]-->

<head>

    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"/>
    <meta charset="utf-8"/>
    <meta name="description" content="">
    <meta name="author" content="">

    <title>NewsMagazine</title>

    <link rel="stylesheet" type="text/css" media="screen" href="style.css" />

    <!--[if lt IE 9]>
	    <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

</head>

<body id="top">

<!-- header
============================================================================= -->

<div id="header-wrap"><header>

       <hgroup style="left: 10px; top: 85px; width: 257px">
            <h1>News Magazine</h1>
            <h3>News for you</h3>
        </hgroup>

		<nav>
		    <ul>
			    <li><a href="index.jsp">Home</a><span></span></li>
			    <li id="current"><a href="SignIn.jsp">Sign In</a><span></span></li>
			    <li><a href="index.jsp">Support</a><span></span></li>
			    <li><a href="index.jsp">About</a><span></span></li>
		    </ul>
	    </nav>

        <div class="subscribe">
            <span>Subscribe:</span> <a href="#">Email</a> | <a href="#">RSS</a>
        </div>


        <form id="quick-search" method="get" action="article.jsp">
            <fieldset class="search">
                <label for="qsearch">Search:</label>
                <input class="tbox" id="qsearch" type="text" name="qsearch" value="Search..." title="Start typing and hit ENTER" />
                <button class="btn" title="Submit Search">Search</button>
            </fieldset>
        </form>

	<!-- /header -->
</header></div>

<!-- Content
============================================================================== -->
<div id="content-wrap">

    <!-- main -->
    <section id="main" class="auto-style1">

        <article class="post">

        <form action="article.jsp" method="post" id="commentform">

            <div>
			    <label for="name">Enter article ID <span>*</span></label>
                            <select name="groups">
                                <option></option>
                                <option></option>
                            </select>
            </div>
            <div class="no-border">
                <input class="button" type="submit" value="Submit" tabindex="5"/>
            </div>
        </form>

            
    <!-- /main -->
    </section>

    <!-- sidebar -->
</script>
<!-- extra
============================================================================== -->
<div id="extra-wrap"><div id="extra" class="clearfix">

    <div class="xcol">

		    <h3>Contact Info</h3>

		    <p>
		    <strong>Phone: </strong>+1234567<br/>
		    <strong>Fax: </strong>+123456789
		    </p>

		    <p><strong>Address: </strong>123 Put Your Address Here</p>
            <p><strong>E-mail: </strong>me@jungleland.com</p>
		    <p>Want more info - go to our <a href="#">contact page</a></p>

            <h3>Follow Us</h3>

	        <div class="footer-list social">
			    <ul>
				    <li class="facebook"><a href="index.html">Facebook</a></li>
				    <li class="twitter"><a href="index.html">Twitter</a></li>
				    <li class="googleplus"><a href="index.html">Google+</a></li>
				    <li class="email"><a href="index.html">Email</a></li>
				    <li class="rss"><a href="index.html">RSS Feed</a></li>
			    </ul>
		    </div>

	</div>

    <div class="xcol">

            <h3>Site Links</h3>

	        <div class="footer-list">
			    <ul>
				    <li><a href="index.html">Home</a></li>
				    <li><a href="index.html">Style Demo</a></li>
				    <li><a href="index.html">Blog</a></li>
				    <li><a href="index.html">Archive</a></li>
				    <li><a href="index.html">About</a></li>
                    <li><a href="index.html">Contact</a></li>
                    <li><a href="index.html">Site Map</a></li>
			    </ul>
		    </div>

            <h3>Credits</h3>

            <div class="footer-list">
			    <ul>
				    <li><a rel="nofollow" href="http://icondock.com/free/vector-social-media-icons">
						Social Media Icons by Icondock
				        </a>
				    </li>
                    <li><a rel="nofollow" href="http://iconsweets2.com/">
						Free Icons by Yummygum
				        </a>
				    </li>
                    <li><a rel="nofollow" href="http://www.pdphoto.org/">
                        Free Photos by PDPhoto
                        </a>
                    </li>
			    </ul>
		    </div>
    </div>

    <div class="xcol">

            <h3>Archives</h3>

	        <div class="footer-list">
			    <ul>
				    <li><a href="index.html">December 2011</a></li>
				    <li><a href="index.html">November 2011</a></li>
				    <li><a href="index.html">October 2011</a></li>
				    <li><a href="index.html">September 2011</a></li>
                    <li><a href="index.html">August 2011</a></li>
                    <li><a href="index.html">July 2011</a></li>
			    </ul>
		    </div>

	</div>
</div></div>

<!-- footer
============================================================================== -->
<footer class="clearfix">

	<p class="footer-left">
		&copy; 2014 Jungleland &bull; 
		Design by <a href="http://www.styleshout.com/">styleshout</a> 
	</p>

	<p class="footer-right">
	   <a href="index.html">Home</a> |
		<a href="index.html">Sitemap</a> |
		<a href="index.html">RSS Feed</a> |
      <a class="back-to-top" href="#top">Back to Top</a>
    </p>

<!-- /footer -->
</footer>

<!-- scripts
============================================================================== -->
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.6/jquery.min.js"></script>
<script>window.jQuery || document.write('<script src="js/jquery-1.6.1.min.js"><\/script>')</script>

<script src="js/scrollToTop.js"></script>

</body>
</html>

