<%-- 
    Document   : Group
    Created on : May 20, 2014, 11:59:34 AM
    Author     : Sana
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<style type="text/css">
    .auto-style1 {
        margin-right: 297px;
    }
</style>
<!--[if IE 7 ]>    <html class="ie7 oldie"> <![endif]-->
<!--[if IE 8 ]>    <html class="ie8 oldie"> <![endif]-->
<!--[if IE 9 ]>    <html class="ie9"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--> <html> <!--<![endif]-->

    <head>

        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"/>
        <meta charset="utf-8"/>
        <meta name="description" content="">
        <meta name="author" content="">

        <title>NewsMagazine</title>

        <link rel="stylesheet" type="text/css" media="screen" href="style.css" />

        <!--[if lt IE 9]>
                <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
        <![endif]-->

    </head>

    <body id="top">

        <!-- header
        ============================================================================= -->

        <div id="header-wrap"><header>

                <hgroup style="left: 10px; top: 85px; width: 257px">
                    <h1>News Magazine</h1>
                    <h3>News for you</h3>
                </hgroup>

                <nav>
                    <ul>
                        <li><a href="index.jsp">Home</a><span></span></li>
                        <li id="current"><a href="SignIn.jsp">Sign In</a><span></span></li>
                        <li><a href="index.jsp">Support</a><span></span></li>
                        <li><a href="index.jsp">About</a><span></span></li>
                    </ul>
                </nav>

                <div class="subscribe">
                    <span>Subscribe:</span> <a href="#">Email</a> | <a href="#">RSS</a>
                </div>

                <form id="quick-search" method="get" action="group.jsp">
                    <fieldset class="search">
                        <label for="qsearch">Search:</label>
                        <input class="tbox" id="qsearch" type="text" name="qsearch" value="Search..." title="Start typing and hit ENTER" />
                        <button class="btn" title="Submit Search">Search</button>
                    </fieldset>
                </form>

                <!-- /header -->
            </header></div>

        <!-- Content
        ============================================================================== -->
        <div id="content-wrap">

            <!-- main -->
            <section id="main" class="auto-style1">

                <article class="post">

                    <form action="./Group" method="POST">

                        <div>
                            <label for="name">Enter group ID <span>*</span></label>
                            <form action="./Group" method="POST"> 
                                <select name="groupId">
                                    <option value="1">1</option>
                                    <option value="2">2</option>
                                    <option value="3">3</option>
                                    <option value="4">4</option>
                                    <option value="5">5</option>
                                </select>
                                <div class="no-border">
                                    <input type="submit" name="action" value="display"/>
                                </div>
                        </div>
                    </form>
                    <br/>
                    <table>
                        <th>Group information</th>

                        <tr>
                            <td> 
                                Id
                            </td> 
                            <td> 
                                ${GroupInfo.getGroupId()}
                            </td>  
                        </tr> 
                        <tr>
                            <td> 
                                Name
                            </td> 
                            <td> 
                                ${GroupInfo.getGroupName()}
                            </td>  
                        </tr> 
                        <tr>
                            <td> 
                                news Count
                            </td> 
                            <td> 
                                ${GroupInfo.getNewsCount()}
                            </td>  
                        </tr> 
                        <tr>
                            <td> 
                                first News
                            </td> 
                            <td> 
                                ${GroupInfo.getFirstNews()}
                            </td>  
                        </tr> 
                        <tr>
                            <td> 
                                last News
                            </td> 
                            <td> 
                                ${GroupInfo.getLastNews()}
                            </td>  
                        </tr> 
                    </table>

                    <!-- /main -->
            </section>

            <!-- footer
            ============================================================================== -->
            <footer class="clearfix">

                <p class="footer-left">
                    &copy; 2014 Jungleland &bull; 
                    Design by <a href="http://www.styleshout.com/">styleshout</a> 
                </p>

                <p class="footer-right">
                    <a href="index.html">Home</a> |
                    <a href="index.html">Sitemap</a> |
                    <a href="index.html">RSS Feed</a> |
                    <a class="back-to-top" href="#top">Back to Top</a>
                </p>

                <!-- /footer -->
            </footer>

            <!-- scripts
            ============================================================================== -->
            <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.6/jquery.min.js"></script>
            <script>window.jQuery || document.write('<script src="js/jquery-1.6.1.min.js"><\/script>')</script>

            <script src="js/scrollToTop.js"></script>

    </body>
</html>