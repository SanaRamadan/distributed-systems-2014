<%-- 
    Document   : Post
    Created on : May 20, 2014, 3:44:37 PM
    Author     : Bassam
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<style type="text/css">
.auto-style1 {
	margin-right: 297px;
}
</style>
<!--[if IE 7 ]>    <html class="ie7 oldie"> <![endif]-->
<!--[if IE 8 ]>    <html class="ie8 oldie"> <![endif]-->
<!--[if IE 9 ]>    <html class="ie9"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--> <html> <!--<![endif]-->

<head>

    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"/>
    <meta charset="utf-8"/>
    <meta name="description" content="">
    <meta name="author" content="">

    <title>NewsMagazine</title>

    <link rel="stylesheet" type="text/css" media="screen" href="style.css" />

    <!--[if lt IE 9]>
	    <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->
     <script src="http://code.jquery.com/jquery-latest.js">   
        </script>
        <script>
            $(document).ready(function() {                        
                $('#submit').click(function(event) {  
                    var articlename=$('#website').val();
                 $.get('PostServlet',function(responseText) { 
                        $('#responsetext').text(responseText);         
                    });        
                });
            });
        </script>
</head>

<body id="top">

<!-- header
============================================================================= -->

<div id="header-wrap"><header>

                 <hgroup style="left: 10px; top: 85px; width: 257px">
            <h1>News Magazine</h1>
            <h3>News for you</h3>
        </hgroup>

		<nav>
		    <ul>
			    <li><a href="index.jsp">Home</a><span></span></li>
			    <li id="current"><a href="SignIn.jsp">Sign In</a><span></span></li>
			    <li><a href="index.jsp">Support</a><span></span></li>
			    <li><a href="index.jsp">About</a><span></span></li>
		    </ul>
	    </nav>


        <div class="subscribe">
            <span>Subscribe:</span> <a href="#">Email</a> | <a href="#">RSS</a>
        </div>


        <form id="quick-search" method="get" action="index.html">
            <fieldset class="search">
                <label for="qsearch">Search:</label>
                <input class="tbox" id="qsearch" type="text" name="qsearch" value="Search..." title="Start typing and hit ENTER" />
                <button class="btn" title="Submit Search">Search</button>
            </fieldset>
        </form>

	<!-- /header -->
</header></div>

<!-- Content
============================================================================== -->
<div id="content-wrap">

    <!-- main -->
    <section id="main" class="auto-style1">

        <article class="post">
            <h2><a href="index.html">Enter details here</a></h2>
        <form action="Post.jsp" method="get" id="commentform">
            <div>
				<label for="website">Article title</label>
				<input id="website" name="website" value="Your Website" type="text" tabindex="3" />
			</div>
            <div>
				<label for="message">Article body<span>*</span></label>
				<textarea id="message" name="message" rows="10" cols="18" tabindex="4"></textarea>
			</div>
            <div class="no-border">
                 <input class="button" id="submit" name="submit" type="submit" value="Submit" tabindex="5" />
            </div>
            <div class="no-border" id="responsetext" name="responsetext">
                Text will load here
            </div>
     
        </form>


    <!-- /main -->
    </section>

    <!-- sidebar -->
 
<!-- footer
============================================================================== -->
<footer class="clearfix">

	<p class="footer-left">
		&copy; 2014 Jungleland &bull; 
		Design by <a href="http://www.styleshout.com/">styleshout</a> 
	</p>

	<p class="footer-right">
	   <a href="index.html">Home</a> |
		<a href="index.html">Sitemap</a> |
		<a href="index.html">RSS Feed</a> |
      <a class="back-to-top" href="#top">Back to Top</a>
    </p>

<!-- /footer -->
</footer>

<!-- scripts
============================================================================== -->
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.6/jquery.min.js"></script>
<script>window.jQuery || document.write('<script src="js/jquery-1.6.1.min.js"><\/script>')</script>

<script src="js/scrollToTop.js"></script>

</body>
</html>
