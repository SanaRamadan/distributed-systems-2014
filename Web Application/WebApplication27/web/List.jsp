<%-- 
    Document   : style
    Created on : May 20, 2014, 1:31:48 PM
    Author     : Bassam
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<!--[if IE 7 ]>    <html class="ie7 oldie"> <![endif]-->
<!--[if IE 8 ]>    <html class="ie8 oldie"> <![endif]-->
<!--[if IE 9 ]>    <html class="ie9"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--> <html> <!--<![endif]-->

<head>

    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"/>
    <meta charset="utf-8"/>
    <meta name="description" content="">
    <meta name="author" content="">

    <title>NewsMagazine</title>

    <link rel="stylesheet" type="text/css" media="screen" href="style.css" />

    <!--[if lt IE 9]>
	    <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

</head>

<body id="top">

<!-- header
============================================================================= -->

<div id="header-wrap"><header>

        <hgroup style="left: 10px; top: 85px; width: 257px">
            <h1>News Magazine</h1>
            <h3>News for you</h3>
        </hgroup>

		<nav>
		    <ul>
			    <li id="current"><a href="index.jsp">Home</a><span></span></li>
			    <li><a href="SignIn.jsp">Sign In</a><span></span></li>
			    <li><a href="index.jsp">Support</a><span></span></li>
			    <li><a href="index.jsp">About</a><span></span></li>
		    </ul>
	    </nav>

        <div class="subscribe">
            <span>Subscribe:</span> <a href="#">Email</a> | <a href="#">RSS</a>
        </div>


        <form id="quick-search" method="get" action="List.jsp">
            <fieldset class="search">
                <label for="qsearch">Search:</label>
                <input class="tbox" id="qsearch" type="text" name="qsearch" value="Search..." title="Start typing and hit ENTER" />
                <button class="btn" title="Submit Search">Search</button>
            </fieldset>
        </form>

	<!-- /header -->
</header></div>

<!-- Content
============================================================================== -->

<div id="content-wrap">

    <!-- main -->
    <section id="main">
        
        <div class="no-border">
                <input class="button" name="action" type="submit" value="List" tabindex="5"/>
        </div>
        <h3>Groups</h3>
            <table>
				<tr>
					<th>Group name</th>
					<th>First article number</th>
					<th>Last article number</th>
				</tr>
				<c:forEach items="${news}" var="new">
                                <tr>
                                   <td>${new.NEWS_ID}</td>
                                   <td>${new.NEWS_DATE}</td>
                                   <td>${new.NEWS_HEAD}</td>
                                </tr>
                                </c:forEach>
			</table>
    <!-- /main -->
    </section>
<!-- footer
============================================================================== -->
<footer class="clearfix">

	<p class="footer-left">
		&copy; 2014 Jungleland &bull; 
		Design by <a href="http://www.styleshout.com/">styleshout</a> 
	</p>

	<p class="footer-right">
	   	<a href="index.html">Home</a> |
		<a href="index.html">Sitemap</a> |
		<a href="index.html">RSS Feed</a> |
        <a class="back-to-top" href="#top">Back to Top</a>
    </p>

<!-- /footer -->
</footer>

<!-- scripts
============================================================================== -->
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.6/jquery.min.js"></script>
<script>window.jQuery || document.write('<script src="js/jquery-1.6.1.min.js"><\/script>')</script>

<script src="js/scrollToTop.js"></script>

</body>
</html>
