/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package entities;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

/**
 *
 * @author Sana
 */
@Entity
@Table(name = "GROUP_PERMISSIONS")
@NamedQueries({
    @NamedQuery(name = "GroupPermissions.findAll", query = "SELECT g FROM GroupPermissions g"),
    @NamedQuery(name = "GroupPermissions.findByPermissionId", query = "SELECT g FROM GroupPermissions g WHERE g.permissionId = :permissionId"),
    @NamedQuery(name = "GroupPermissions.findByReadPermission", query = "SELECT g FROM GroupPermissions g WHERE g.readPermission = :readPermission"),
    @NamedQuery(name = "GroupPermissions.findByWritePermission", query = "SELECT g FROM GroupPermissions g WHERE g.writePermission = :writePermission")})
public class GroupPermissions implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "PERMISSION_ID")
    private Integer permissionId;
    @Basic(optional = false)
    @NotNull
    @Column(name = "READ_PERMISSION")
    private Boolean readPermission;
    @Basic(optional = false)
    @NotNull
    @Column(name = "WRITE_PERMISSION")
    private Boolean writePermission;
    @JoinColumn(name = "GROUP_ID", referencedColumnName = "GROUP_ID")
    @ManyToOne(optional = false)
    private NetworkGroups groupId;
    @JoinColumn(name = "USER_ID", referencedColumnName = "USER_ID")
    @ManyToOne(optional = false)
    private NetworkUsers userId;

    public GroupPermissions() {
    }

    public GroupPermissions(Integer permissionId) {
        this.permissionId = permissionId;
    }

    public GroupPermissions(Integer permissionId, Boolean readPermission, Boolean writePermission) {
        this.permissionId = permissionId;
        this.readPermission = readPermission;
        this.writePermission = writePermission;
    }

    public Integer getPermissionId() {
        return permissionId;
    }

    public void setPermissionId(Integer permissionId) {
        this.permissionId = permissionId;
    }

    public Boolean getReadPermission() {
        return readPermission;
    }

    public void setReadPermission(Boolean readPermission) {
        this.readPermission = readPermission;
    }

    public Boolean getWritePermission() {
        return writePermission;
    }

    public void setWritePermission(Boolean writePermission) {
        this.writePermission = writePermission;
    }

    public NetworkGroups getGroupId() {
        return groupId;
    }

    public void setGroupId(NetworkGroups groupId) {
        this.groupId = groupId;
    }

    public NetworkUsers getUserId() {
        return userId;
    }

    public void setUserId(NetworkUsers userId) {
        this.userId = userId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (permissionId != null ? permissionId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof GroupPermissions)) {
            return false;
        }
        GroupPermissions other = (GroupPermissions) object;
        if ((this.permissionId == null && other.permissionId != null) || (this.permissionId != null && !this.permissionId.equals(other.permissionId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entities.GroupPermissions[ permissionId=" + permissionId + " ]";
    }
    
}
