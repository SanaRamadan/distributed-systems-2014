/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package entities;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author Sana
 */
@Entity
@Table(name = "NEWS")
@NamedQueries({
    @NamedQuery(name = "News.findAll", query = "SELECT n FROM News n"),
    @NamedQuery(name = "News.findByNewsId", query = "SELECT n FROM News n WHERE n.newsId = :newsId"),
    @NamedQuery(name = "News.findByNewsDate", query = "SELECT n FROM News n WHERE n.newsDate = :newsDate"),
    @NamedQuery(name = "News.findByNewsHead", query = "SELECT n FROM News n WHERE n.newsHead = :newsHead"),
    @NamedQuery(name = "News.findByNewsBody", query = "SELECT n FROM News n WHERE n.newsBody = :newsBody")})
public class News implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "NEWS_ID")
    private Integer newsId;
    @Basic(optional = false)
    @NotNull
    @Column(name = "NEWS_DATE")
    @Temporal(TemporalType.DATE)
    private Date newsDate;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 64)
    @Column(name = "NEWS_HEAD")
    private String newsHead;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 256)
    @Column(name = "NEWS_BODY")
    private String newsBody;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "newsId")
    private List<NewsPointer> newsPointerList;
    @JoinColumn(name = "GROUP_ID", referencedColumnName = "GROUP_ID")
    @ManyToOne(optional = false)
    private NetworkGroups groupId;
    @JoinColumn(name = "USER_ID", referencedColumnName = "USER_ID")
    @ManyToOne(optional = false)
    private NetworkUsers userId;

    public News() {
    }

    public News(Integer newsId) {
        this.newsId = newsId;
    }

    public News(Integer newsId, Date newsDate, String newsHead, String newsBody) {
        this.newsId = newsId;
        this.newsDate = newsDate;
        this.newsHead = newsHead;
        this.newsBody = newsBody;
    }

    public Integer getNewsId() {
        return newsId;
    }

    public void setNewsId(Integer newsId) {
        this.newsId = newsId;
    }

    public Date getNewsDate() {
        return newsDate;
    }

    public void setNewsDate(Date newsDate) {
        this.newsDate = newsDate;
    }

    public String getNewsHead() {
        return newsHead;
    }

    public void setNewsHead(String newsHead) {
        this.newsHead = newsHead;
    }

    public String getNewsBody() {
        return newsBody;
    }

    public void setNewsBody(String newsBody) {
        this.newsBody = newsBody;
    }

    public List<NewsPointer> getNewsPointerList() {
        return newsPointerList;
    }

    public void setNewsPointerList(List<NewsPointer> newsPointerList) {
        this.newsPointerList = newsPointerList;
    }

    public NetworkGroups getGroupId() {
        return groupId;
    }

    public void setGroupId(NetworkGroups groupId) {
        this.groupId = groupId;
    }

    public NetworkUsers getUserId() {
        return userId;
    }

    public void setUserId(NetworkUsers userId) {
        this.userId = userId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (newsId != null ? newsId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof News)) {
            return false;
        }
        News other = (News) object;
        if ((this.newsId == null && other.newsId != null) || (this.newsId != null && !this.newsId.equals(other.newsId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entities.News[ newsId=" + newsId + " ]";
    }
    
}
