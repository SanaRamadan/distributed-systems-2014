/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sessions;

import entities.NewsPointer;
import javax.ejb.LocalBean;
import javax.persistence.EntityManager;

/**
 *
 * @author Sana
 */
@LocalBean
public interface NewsPointerSessionLocal {

    abstract EntityManager getEntityManager();

    public void create(NewsPointer entity);

    public void edit(NewsPointer entity);

    public NewsPointer find(int id);
}
