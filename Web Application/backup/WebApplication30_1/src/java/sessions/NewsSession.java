/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sessions;

import entities.News;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author Sana
 */
@Stateless
public class NewsSession implements NewsSessionLocal {

    @PersistenceContext(unitName = "WebApplication30PU")
    private EntityManager em;

    @Override
    public String createe() {
        return "createe";
    }

    @Override
    public void create(News entity) {
        getEntityManager().persist(entity);
    }

    @Override
    public void edit(News entity) {
        getEntityManager().merge(entity);
    }

    @Override
    public void remove(News entity) {
        getEntityManager().remove(getEntityManager().merge(entity));
    }

    @Override
    public News find(int id) {
        return getEntityManager().find(News.class, id);
    }

    @Override
    public List<News> findAll() {
        javax.persistence.criteria.CriteriaQuery cq = getEntityManager().getCriteriaBuilder().createQuery();
        cq.select(cq.from(News.class));
        return getEntityManager().createQuery(cq).getResultList();
    }

    @Override
    public int count() {
        javax.persistence.criteria.CriteriaQuery cq = getEntityManager().getCriteriaBuilder().createQuery();
        javax.persistence.criteria.Root<News> rt = cq.from(News.class);
        cq.select(getEntityManager().getCriteriaBuilder().count(rt));
        javax.persistence.Query q = getEntityManager().createQuery(cq);
        return ((Long) q.getSingleResult()).intValue();
    }

    @Override
    public EntityManager getEntityManager() {
        return em;
    }

}
