/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sessions;

import entities.NetworkGroups;
import entities.NetworkUsers;
import java.util.List;
import javax.ejb.LocalBean;
import javax.persistence.EntityManager;

/**
 *
 * @author Sana
 */
@LocalBean
public interface NetworkUsersSessionLocal {

    abstract EntityManager getEntityManager();

    public void create(NetworkUsers entity);
 
}
