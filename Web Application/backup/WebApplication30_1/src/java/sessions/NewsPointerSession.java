/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sessions;

import entities.News;
import entities.NewsPointer;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author Sana
 */
@Stateless
public class NewsPointerSession implements NewsPointerSessionLocal {

    @PersistenceContext(unitName = "WebApplication30PU")
    private EntityManager em;

    @Override
    public void create(NewsPointer entity) {
        getEntityManager().persist(entity);
    }

    @Override
    public void edit(NewsPointer entity) {
        getEntityManager().merge(entity);
    }

    @Override
    public NewsPointer find(int id) {
        return getEntityManager().find(NewsPointer.class, id);
    }

    @Override
    public EntityManager getEntityManager() {
        return em;
    } 

}
