/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package client;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.Socket;
import java.util.Scanner;

/**
 *
 * @author Sana
 */
public class Client {

    private static final int BUFSIZE = 700;
    public static String HOST = "localhost";
    public static int PORT = 1119;
    private static Scanner scanIn = new Scanner(System.in);

    public static void main(String[] args) throws IOException, ClassNotFoundException {

        Socket socket = new Socket(HOST, PORT);
        System.out.println("Connected to server...sending command");

        InputStream in;
        OutputStream out;
        while (true) {

            in = socket.getInputStream();
            out = socket.getOutputStream();

            System.out.println("\n"
                    + "Enter the command id\n");
            System.out.println(""
                    + "LIST command (1)\n"
                    + "GROUP command (2) \n"
                    + "HELP command (3) \n"
                    + "NEXT command (4) \n"
                    + "PREVIOUS command (5) \n"
                    + "POST command (6) \n"
                    + "POST command with News information(7) \n"
                    + "QUIT command (8) \n"
                    + "ARTICLE command (9) \n"
                    + "ARTICLE command with select spesific News(10) \n");

            int commandId = scanIn.nextInt();
            byte[] data = handleCommand(commandId);
            if (data == null) {
                break;
            }
            out.write(data); // Send the encoded command to the server

            // Receive the same string back from the server
            int totalBytesRcvd = 0; // Total bytes received so far
            int bytesRcvd; // Bytes received in last read

            byte[] receivedData = new byte[BUFSIZE];
            while (totalBytesRcvd == 0) {
                if ((bytesRcvd = in.read(receivedData)) != -1) {
                    totalBytesRcvd += bytesRcvd;
                }
            }
            System.out.println(new String(receivedData));

        }
        socket.close();
        //  handleRecievedCommand(commandId, data);
        scanIn.close();

    }

    private static byte[] handleCommand(int commandNum) {
        byte[] data = null;
        String command = "";
        String newsHead = "";
        String newsBody = "";
        int userId = 0;
        int groupId = 0;
        int newsId = 0;

        switch (commandNum) {
            case 1:
                System.out.println("Enter the user id");
                userId = scanIn.nextInt();
                command = "LIST " + userId + " ";
                data = command.getBytes();
                return data;

            case 2:
                System.out.println("Enter the group id");
                groupId = scanIn.nextInt();
                command = "GROUP " + groupId + " ";
                data = command.getBytes();
                return data;

            case 3:
                command = "HELP ";
                data = command.getBytes();
                return data;

            case 4:
                System.out.println("Enter the user id");
                userId = scanIn.nextInt();
                command = "NEXT " + userId + " ";
                data = command.getBytes();
                return data;

            case 5:
                System.out.println("Enter the user id");
                userId = scanIn.nextInt();
                command = "PREVIOUS " + userId + " ";
                data = command.getBytes();
                return data;

            case 6:
                System.out.println("Enter the user id");
                userId = scanIn.nextInt();
                System.out.println("Enter the group id");
                groupId = scanIn.nextInt();
                command = "POST " + userId + " " + groupId + " ";
                data = command.getBytes();
                return data;

            case 7:
                System.out.println("Enter the user id");
                userId = scanIn.nextInt();
                System.out.println("Enter the group id");
                groupId = scanIn.nextInt();
                System.out.println("Enter the news id");
                newsId = scanIn.nextInt();
                System.out.println("Enter the news head");
                scanIn.nextLine();
                String temp;
                while (!(temp = scanIn.nextLine()).equals("")) {
                    newsHead += temp + " ";
                }
                System.out.println("Enter the news body");
                while (!(temp = scanIn.next()).equals(".")) {
                    newsBody += temp + " ";
                }
                command = "POST " + userId + " " + groupId
                        + " " + newsId + " " + newsHead + " " + newsBody + " ";
                data = command.getBytes();
                return data;

            case 8:
                return data;

            case 9:
                System.out.println("Enter the user id");
                userId = scanIn.nextInt();
                command = "ARTICLE " + userId + " ";
                data = command.getBytes();
                return data;

            case 10:
                System.out.println("Enter the user id");
                userId = scanIn.nextInt();
                System.out.println("Enter the news id");
                newsId = scanIn.nextInt();
                command = "ARTICLE " + userId + " " + newsId + " ";
                data = command.getBytes();
                return data;

        }
        return null;
    }
}
