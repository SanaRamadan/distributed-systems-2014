/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package command_model;

import java.io.Serializable;
import java.util.List;

/**
 *
 * @author Douaa-pc
 */
public class HELPCommand implements Serializable {

    private static final long serialVersionUID = 1L;

    private List<String> commands;

    public HELPCommand() {
    }

    public List<String> getCommands() {
        return commands;
    }

    public void setCommands(List<String> commands) {
        this.commands = commands;
    }
}
