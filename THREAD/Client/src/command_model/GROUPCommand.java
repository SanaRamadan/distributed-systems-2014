/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package command_model;

import java.io.Serializable;

/**
 *
 * @author Douaa-pc
 */
public class GROUPCommand implements Serializable {

    private static final long serialVersionUID = 1L;

    private int groupId;
    private int newsCount;
    private int firstNews;
    private int lastNews;
    

    public GROUPCommand(int groupId) {
        this.groupId = groupId;
    }
 
    public int getGroupId() {
        return groupId;
    }

    public void setGroupId(int groupId) {
        this.groupId = groupId;
    }

    public int getNewsCount() {
        return newsCount;
    }

    public void setNewsCount(int newsCount) {
        this.newsCount = newsCount;
    }

    public int getFirstNews() {
        return firstNews;
    }

    public void setFirstNews(int firstNews) {
        this.firstNews = firstNews;
    }

    public int getLastNews() {
        return lastNews;
    }

    public void setLastNews(int lastNews) {
        this.lastNews = lastNews;
    }

}
