/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package command_model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Douaa-pc
 */
public class LISTCommand implements Serializable {

    private static final long serialVersionUID = 1L;

    private int userId;
    private ArrayList<Group> groups;

    public LISTCommand(int userId) {
        this.userId = userId;
        this.groups = new ArrayList<>();
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public ArrayList<Group> getGroups() {
        return groups;
    }

    public void setGroups(ArrayList<Group> groups) {
        this.groups = groups;
    }

    public class Group implements Serializable {

        private static final long serialVersionUID = 9641L;

        private int groupId;
        private String groupName;
        private int firstNews;
        private int lastNews;
        private boolean writePermission;

        public Group(int groupId, String groupName, int firstNews, int lastNews, boolean writePermission) {
            this.groupId = groupId;
            this.groupName = groupName;
            this.firstNews = firstNews;
            this.lastNews = lastNews;
            this.writePermission = writePermission;
        }

        public int getGroupId() {
            return groupId;
        }

        public void setGroupId(int groupId) {
            this.groupId = groupId;
        }

        public String getGroupName() {
            return groupName;
        }

        public void setGroupName(String groupName) {
            this.groupName = groupName;
        }

        public boolean isWritePermission() {
            return writePermission;
        }

        public void setWritePermission(boolean writePermission) {
            this.writePermission = writePermission;
        }

        public int getFirstNews() {
            return firstNews;
        }

        public void setFirstNews(int firstNews) {
            this.firstNews = firstNews;
        }

        public int getLastNews() {
            return lastNews;
        }

        public void setLastNews(int lastNews) {
            this.lastNews = lastNews;
        }
    }

}
