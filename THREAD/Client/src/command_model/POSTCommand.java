/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package command_model;

import java.io.Serializable; 

/**
 *
 * @author Douaa-pc
 */
public class POSTCommand implements Serializable {

    private static final long serialVersionUID = 1L;

    private static final String WRITE_CODE = "340";
    private static final String READ_CODE = "440"; 
    private int userId;
    private int newsId;
    private int groupId;
    private String newsBody;
    private String newsHead;
    private String writePermission;

    
    public POSTCommand(int userId, int groupId) {
        this.userId = userId;
        this.groupId = groupId; 
    }
 
    public static String getWRITE_CODE() {
        return WRITE_CODE;
    }

    public static String getREAD_CODE() {
        return READ_CODE;
    }

    public int getGroupId() {
        return groupId;
    }

    public void setGroupId(int groupId) {
        this.groupId = groupId;
    }

    public int getNewsId() {
        return newsId;
    }

    public void setNewsId(int newsId) {
        this.newsId = newsId;
    }

    public String getNewsBody() {
        return newsBody;
    }

    public void setNewsBody(String newsBody) {
        this.newsBody = newsBody;
    }

    public String getNewsHead() {
        return newsHead;
    }

    public void setNewsHead(String newsHead) {
        this.newsHead = newsHead;
    }
 

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public String isWritePermission() {
        return writePermission;
    }

    public void setWritePermission(String writePermission) {
        this.writePermission = writePermission;
    }
}
