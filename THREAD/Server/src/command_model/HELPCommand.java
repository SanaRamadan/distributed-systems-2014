/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package command_model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import model.Command;

/**
 *
 * @author Douaa-pc
 */
public class HELPCommand implements Serializable {

    private static final long serialVersionUID = 1L;

    private List<String> commands;

    public HELPCommand() {
    }

    public HELPCommand execute() {

        commands = new ArrayList<>();

        commands.add(LISTCommand.commandInformation());
        commands.add(GROUPCommand.commandInformation());
        commands.add(HELPCommand.commandInformation());
        commands.add(NEXTCommand.commandInformation());
        commands.add(PREVIOUSCommand.commandInformation());
        commands.add(POSTCommand.commandInformation());
        commands.add(QUITCommand.commandInformation());
        commands.add(ARTICLECommand.commandInformation());

        return this;
    }

    public static String commandInformation() {
        return new Command(2, "HELP", "Return information about each command.").toString();
    }

    @Override
    public String toString() {
        return "HELPCommand{" + "commands are\n" + commands + '}';
    }

    public List<String> getCommands() {
        return commands;
    }

    public void setCommands(List<String> commands) {
        this.commands = commands;
    }
    
}
