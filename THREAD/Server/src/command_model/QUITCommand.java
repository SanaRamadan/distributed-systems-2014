/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package command_model;

import java.io.Serializable;
import model.Command;

/**
 *
 * @author Douaa-pc
 */
public class QUITCommand implements Serializable {

    private static final long serialVersionUID = 1L;

    private boolean state;

    public QUITCommand() {
        state = true;
    }

    public QUITCommand execute() {
        state = false;
        return this;
    }

    public static String commandInformation() {
        return new Command(6, "QUIT", "Disconnection with Server.").toString();
    }

    public boolean isState() {
        return state;
    }
}
