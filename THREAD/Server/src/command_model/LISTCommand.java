/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package command_model;

import adapter.GroupPermissionAdapter;
import adapter.NetworkGroupsAdapter;
import adapter.NewsAdapter;
import java.io.Serializable;
import java.util.ArrayList;
import model.Command;
import model.NetworkGroup;

/**
 *
 * @author Douaa-pc
 */
public class LISTCommand implements Serializable {

    private static final long serialVersionUID = 1L;

    private int userId;
    private ArrayList<Group> groups;

    public LISTCommand(int userId) {
        this.userId = userId;
        this.groups = new ArrayList<>();
    }

    public LISTCommand execute() {
        ArrayList<NetworkGroup> groupsList = NetworkGroupsAdapter.getAllGroups();

        for (NetworkGroup g : groupsList) {

            int groupId = g.getGroupId();
            String groupName = g.getGroupName();
            int firstNews = 0, lastNews = 0;
            if (NewsAdapter.getNewsCount(groupId) != 0) {
                firstNews = NewsAdapter.getFirstNews(groupId).getNewsId();
                lastNews = NewsAdapter.getLastNews(groupId).getNewsId();
            }
            boolean writePermission = GroupPermissionAdapter.getGroupWritePermission(userId, groupId);

            Group group = new Group(groupId, groupName, firstNews, lastNews, writePermission);
            groups.add(group);
        }
        return this;
    }

    public static String commandInformation() {
        return new Command(0, "LIST", "Return group's news and number of them.").toString();
    }

    @Override
    public String toString() {
        return "LISTCommand{" + "userId=" + userId + ", groups=" + groups + '}';
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public ArrayList<Group> getGroups() {
        return groups;
    }

    public void setGroups(ArrayList<Group> groups) {
        this.groups = groups;
    }

    public class Group implements Serializable {

        private static final long serialVersionUID = 9641L;

        private int groupId;
        private String groupName;
        private int firstNews;
        private int lastNews;
        private boolean writePermission;

        public Group(int groupId, String groupName, int firstNews, int lastNews, boolean writePermission) {
            this.groupId = groupId;
            this.groupName = groupName;
            this.firstNews = firstNews;
            this.lastNews = lastNews;
            this.writePermission = writePermission;
        }

        @Override
        public String toString() {
            return "\nGroup{" + "groupId=" + groupId + ", groupName=" + groupName + ", firstNews=" + firstNews + ", lastNews=" + lastNews + ", writePermission=" + writePermission + '}';
        }

        public int getGroupId() {
            return groupId;
        }

        public void setGroupId(int groupId) {
            this.groupId = groupId;
        }

        public String getGroupName() {
            return groupName;
        }

        public void setGroupName(String groupName) {
            this.groupName = groupName;
        }

        public boolean isWritePermission() {
            return writePermission;
        }

        public void setWritePermission(boolean writePermission) {
            this.writePermission = writePermission;
        }

        public int getFirstNews() {
            return firstNews;
        }

        public void setFirstNews(int firstNews) {
            this.firstNews = firstNews;
        }

        public int getLastNews() {
            return lastNews;
        }

        public void setLastNews(int lastNews) {
            this.lastNews = lastNews;
        }
    }
}
