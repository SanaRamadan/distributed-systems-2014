/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package command_model;

import adapter.GroupPermissionAdapter;
import adapter.NewsAdapter;
import adapter.NewsPointerAdapter;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import model.Command;
import model.News;
import model.NewsPointer;

/**
 *
 * @author Douaa-pc
 */
public class PREVIOUSCommand implements Serializable {

    private static final long serialVersionUID = 1L;

    private int userId;
    private int newsId;
    private int groupId;
    private String newsBody;
    private String newsHead;

    public PREVIOUSCommand(int userId) {
        this.userId = userId;
    }

    public PREVIOUSCommand() {
    }

    public PREVIOUSCommand execute() {
        int groupId;
        News previousNews;
        NewsPointer pointer = NewsPointerAdapter.getNewsPointer(userId);

        if (pointer == null) {
            groupId = GroupPermissionAdapter.getFirstReadPermission(userId).getGroupId();
            News news;
            news = NewsAdapter.getFirstNews(groupId);

            this.groupId = news.getGroupId();
            this.newsBody = news.getNewsBody();
            this.newsHead = news.getNewsHead();
            this.newsId = news.getNewsId();

            previousNews = NewsAdapter.getPreviousNews(news.getNewsId());
            NewsPointerAdapter.addNewsPointer(previousNews.getNewsId(), userId);
        } else {
            if (pointer.getNewsId() != NewsAdapter.getFirstNews().getNewsId()) {
                previousNews = NewsAdapter.getPreviousNews(pointer.getNewsId());

                this.groupId = previousNews.getGroupId();
                this.newsBody = previousNews.getNewsBody();
                this.newsHead = previousNews.getNewsHead();
                this.newsId = previousNews.getNewsId();

                NewsPointerAdapter.updatePointer(pointer.getPointerId(), previousNews.getNewsId());
            } else {
                News news;
                news = NewsAdapter.getNews(pointer.getNewsId());

                this.groupId = news.getGroupId();
                this.newsBody = news.getNewsBody();
                this.newsHead = news.getNewsHead();
                this.newsId = news.getNewsId();
            }
        }
        return this;
    }

    public static String commandInformation() {
        return new Command(4, "PREVIOUS", "Return previous news.").toString();
    }

    @Override
    public String toString() {
        return "PREVIOUSCommand{" + "userId=" + userId + ", groupId=" + groupId + ", newsId=" + newsId + ", newsHead=" + newsHead + ", newsBody=" + newsBody + '}';
    }

    public int getNewsId() {
        return newsId;
    }

    public void setNewsId(int newsId) {
        this.newsId = newsId;
    }

    public int getGroupId() {
        return groupId;
    }

    public void setGroupId(int groupId) {
        this.groupId = groupId;
    }

    public String getNewsBody() {
        return newsBody;
    }

    public void setNewsBody(String newsBody) {
        this.newsBody = newsBody;
    }

    public String getNewsHead() {
        return newsHead;
    }

    public void setNewsHead(String newsHead) {
        this.newsHead = newsHead;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

}
