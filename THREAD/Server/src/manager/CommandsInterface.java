/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package manager;

import command_model.ARTICLECommand;
import command_model.LISTCommand;
import command_model.NEXTCommand;
import command_model.POSTCommand;
import command_model.PREVIOUSCommand;
import command_model.QUITCommand;
import command_model.GROUPCommand;
import command_model.HELPCommand;
import java.rmi.Remote;
import java.rmi.RemoteException;
import model.NetworkGroup;

/**
 *
 * @author Douaa-pc
 */
public interface CommandsInterface {

    public LISTCommand list(int userId);

    public GROUPCommand group(int groupId);

    public HELPCommand help();

    public NEXTCommand next(int userId);

    public PREVIOUSCommand previous(int userId);

    public POSTCommand post(int userId, int groupId);

    public POSTCommand post(int userId, int groupId, int newsId, String newsHead, String newsBody);

    public QUITCommand quit();

    public ARTICLECommand artical(int userId, int newsId);

    public ARTICLECommand artical(int userId);
}
