/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package manager;

import command_model.ARTICLECommand;
import command_model.GROUPCommand;
import command_model.HELPCommand;
import command_model.LISTCommand;
import command_model.NEXTCommand;
import command_model.POSTCommand;
import command_model.PREVIOUSCommand;
import command_model.QUITCommand;

/**
 *
 * @author Douaa-pc
 */
public class Commands implements CommandsInterface {

    private int dbNO;

    public Commands(int dbNO) {
        this.dbNO = dbNO;
    }

    @Override
    public GROUPCommand group(int groupId) {
        try {

            return new GROUPCommand(groupId).execute();
        } catch (Exception e) {
            throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        }
    }

    @Override
    public LISTCommand list(int userId) {
        try {
            return new LISTCommand(userId).execute();
        } catch (Exception e) {
            throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        }
    }

    @Override
    public HELPCommand help() {
        try {
            return new HELPCommand().execute();
        } catch (Exception e) {
            throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        }
    }

    @Override
    public synchronized NEXTCommand next(int userId) {
        try {
            return new NEXTCommand(userId).execute();
        } catch (Exception e) {
            throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        }
    }

    @Override
    public PREVIOUSCommand previous(int userId) {
        try {
            return new PREVIOUSCommand(userId).execute();
        } catch (Exception e) {
            throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        }
    }

    @Override
    public POSTCommand post(int userId, int groupId) {
        try {
            return new POSTCommand(userId, groupId).execute();
        } catch (Exception e) {
            throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        }
    }

    @Override
    public POSTCommand post(int userId, int groupId, int newsId, String newsHead, String newsBody) {
        try {
            return new POSTCommand(userId, groupId).execute(newsId, newsHead, newsBody);
        } catch (Exception e) {
            throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        }
    }

    @Override
    public QUITCommand quit() {
        try {
            return new QUITCommand().execute();
        } catch (Exception e) {
            throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        }
    }

    @Override
    public ARTICLECommand artical(int userId, int newsId) {
        try {
            return new ARTICLECommand(userId, newsId).execute();
        } catch (Exception e) {
            throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        }
    }

    @Override
    public ARTICLECommand artical(int userId) {
        try {
            return new ARTICLECommand(userId).execute();
        } catch (Exception e) {
            throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        }
    }
}
