/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

/**
 *
 * @author Sana
 */
public class NewsPointer {

    private int pointerId;
    private int userId;
    private int newsId;

    public NewsPointer(int pointerId, int userId, int newsId) {
        this.pointerId = pointerId;
        this.userId = userId;
        this.newsId = newsId;
    }

    public int getPointerId() {
        return pointerId;
    }

    public void setPointerId(int pointerId) {
        this.pointerId = pointerId;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public int getNewsId() {
        return newsId;
    }

    public void setNewsId(int newsId) {
        this.newsId = newsId;
    }
}
