/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

/**
 *
 * @author Sana
 */
public class Command {

    private int commandId;
    private String commandSyntax;
    private String commandDescription;

    public Command(int commandId, String commandSyntax, String commandDescription) {
        this.commandId = commandId;
        this.commandSyntax = commandSyntax;
        this.commandDescription = commandDescription;
    }

    @Override
    public String toString() {
        return "Command{" + "commandId=" + commandId + ", commandSyntax=" + commandSyntax + ", commandDescription=" + commandDescription + "}\n";
    }

    public int getCommandId() {
        return commandId;
    }

    public void setCommandId(int commandId) {
        this.commandId = commandId;
    }

    public String getCommandSyntax() {
        return commandSyntax;
    }

    public void setCommandSyntax(String commandSyntax) {
        this.commandSyntax = commandSyntax;
    }

    public String getCommandDescription() {
        return commandDescription;
    }

    public void setCommandDescription(String commandDescription) {
        this.commandDescription = commandDescription;
    }
}
