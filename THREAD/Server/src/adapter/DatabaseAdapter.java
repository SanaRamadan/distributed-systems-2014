/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package adapter;

import java.sql.Connection;
import java.sql.DriverManager;

/**
 *
 * @author Sana
 */
public abstract class DatabaseAdapter {

    private static final String HOST = "jdbc:derby://localhost:1527/NetworkNews";
    private static final String USER_NAME = "admin";
    private static final String USER_PASSWORD = "admin";
    private static int DB_NO = 1;

    public static void setdbNO(int dbNO) {
        DB_NO = dbNO;
    }

    protected static Connection getConnection() {
        try {
            Class.forName("org.apache.derby.jdbc.ClientDriver");
            return DriverManager.getConnection(HOST + DB_NO, USER_NAME + DB_NO, USER_PASSWORD + DB_NO);
        } catch (Exception e) {
            return null;
        }
    }
}
