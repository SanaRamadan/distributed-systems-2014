/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package adapter;

import static adapter.DatabaseAdapter.getConnection;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import model.GroupPermission;
import model.NetworkGroup;

/**
 *
 * @author Sana
 */
public class GroupPermissionAdapter extends DatabaseAdapter {

    private static final String PERMISSION_ID = "PERMISSION_ID";
    private static final String READ_PERMISSION = "READ_PERMISSION";
    private static final String WRITE_PERMISSION = "WRITE_PERMISSION";
    private static final String USER_ID = "USER_ID";
    private static final String GROUP_ID = "GROUP_ID";
    public static final String TABLE_NAME = "GROUP_PERMISSIONS";

    public static void addPermission(int permissionId, int userId, int groupId, boolean readPermission, boolean writePermission) {
        try {
            Connection conn = getConnection();
            Statement st = conn.createStatement();
            st.executeUpdate("INSERT INTO " + TABLE_NAME + " (" + PERMISSION_ID + "," + READ_PERMISSION + ","
                    + WRITE_PERMISSION + "," + USER_ID + "," + GROUP_ID
                    + ") VALUES(" + permissionId + ", " + readPermission + ", " + writePermission + ", "
                    + userId + ", " + groupId + ")");

            st.close();
        } catch (Exception e) {
            System.out.println("Error - " + e.toString());
        }
    }

    public static GroupPermission getPermission(int userId, int groupId) {
        GroupPermission permission = null;
        try {
            Connection conn = getConnection();
            Statement st = conn.createStatement();
            ResultSet rec = st.executeQuery("SELECT *  FROM " + TABLE_NAME
                    + " WHERE " + USER_ID + "=" + userId + " AND " + GROUP_ID + "=" + groupId);
            rec.next();
            permission = new GroupPermission(rec.getInt(PERMISSION_ID), groupId, userId,
                    rec.getBoolean(READ_PERMISSION), rec.getBoolean(WRITE_PERMISSION));
            st.close();
        } catch (Exception e) {
            System.out.println("Error - " + e.toString());
        }
        return permission;
    }

    public static GroupPermission getFirstReadPermission(int userId) {
        GroupPermission permission = null;
        try {
            Connection conn = getConnection();
            Statement st = conn.createStatement();
            ResultSet rec = st.executeQuery("SELECT *  FROM " + TABLE_NAME
                    + " WHERE " + USER_ID + "=" + userId + " AND " + READ_PERMISSION + "=true "
                    + " ORDER BY " + GROUP_ID);
            rec.next();
            permission = new GroupPermission(rec.getInt(PERMISSION_ID), rec.getInt(GROUP_ID), userId,
                    rec.getBoolean(READ_PERMISSION), rec.getBoolean(WRITE_PERMISSION));

            st.close();
        } catch (Exception e) {
            System.out.println("Error - " + e.toString());
        }
        return permission;
    }

    public static boolean getGroupWritePermission(int userId, int groupId) {
        return getPermission(userId, groupId).isWritePermission();

    }

    public static boolean getGroupReadPermission(int userId, int groupId) {
        return getPermission(userId, groupId).isReadPermission();
    }
}