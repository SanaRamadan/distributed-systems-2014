/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package server;
 
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;
import manager.Commands; 

/**
 *
 * @author Sana
 */
public class Server {

    private static final int BUFSIZE = 700;
    public static String HOST = "localhost";
    public static int PORT = 1119;
    private static Scanner scanIn = new Scanner(System.in);

    public static void main(String[] args) throws IOException {
        int threadPoolSize = 9;

        /*   
         System.out.println("Enter thread pool size");
         threadPoolSize = scanIn.nextInt();
         scanIn.close();
         */
        // Create a server socket to accept client connection requests
        final ServerSocket servSock = new ServerSocket(PORT);

        final Logger logger = Logger.getLogger("practical");

        // Spawn a fixed number of threads to service clients
        for (int i = 0; i < threadPoolSize; i++) {
            Thread thread = new Thread() {
                public void run() {
                    while (true) {
                        try {
                            Socket clntSock = servSock.accept(); // Wait for a connection

                            InputStream in;
                            OutputStream out;
                            while (true) {
                                in = clntSock.getInputStream();
                                out = clntSock.getOutputStream();

                                int recvMsgSize; // Size of received message
                                int totalBytesEchoed = 0; // Bytes received from client
                                byte[] commandBuffer = new byte[BUFSIZE]; // Receive Buffer                             

                                // Receive until client closes connection, indicated by -1
                                while ((recvMsgSize = in.read(commandBuffer)) != -1) {
                                    String command = new String(commandBuffer);
                                    out.write(handleCommand(command));
                                    totalBytesEchoed += recvMsgSize;
                                }
                            }
                        } catch (IOException ex) {
                            logger.log(Level.WARNING, "Client accept failed", ex);

                        }
                    }

                }
            };
            thread.start();

            logger.info("Created and started Thread = " + thread.getName());
        }
    }

    private static byte[] handleCommand(String commandString) throws IOException {
        String commandResult = null;
        byte[] data = null;
        String newsHead = "";
        String newsBody = "";
        int userId = 0;
        int groupId = 0;
        int newsId = 0;

        manager.CommandsInterface command = new Commands(1);
        String[] commandToken = commandString.split(" ");
        switch (commandToken[0]) {
            case "LIST":
                userId = Integer.parseInt(commandToken[1]);
                commandResult = command.list(userId).toString();
                data = commandResult.getBytes();
                return data;

            case "GROUP":
                groupId = Integer.parseInt(commandToken[1]);
                commandResult = command.group(groupId).toString();
                data = commandResult.getBytes();
                return data;

            case "HELP":
                commandResult = command.help().toString();
                data = commandResult.getBytes();
                return data;

            case "NEXT":
                userId = Integer.parseInt(commandToken[1]);
                commandResult = command.next(userId).toString();
                data = commandResult.getBytes();
                return data;

            case "PREVIOUS":
                userId = Integer.parseInt(commandToken[1]);
                commandResult = command.previous(userId).toString();
                data = commandResult.getBytes();
                return data;

            case "POST":
                userId = Integer.parseInt(commandToken[1]);
                groupId = Integer.parseInt(commandToken[2]);
                if (commandToken.length == 4) {
                    commandResult = command.post(userId, groupId).toString();
                } else {
                    newsId = Integer.parseInt(commandToken[3]);
                    newsHead = commandToken[4];
                    newsBody = commandToken[5];
                    commandResult = command.post(userId, groupId, newsId, newsHead, newsBody).toString();
                }
                data = commandResult.getBytes();
                return data;

            case "QUIT":
                return data;

            case "ARTICLE":
                 userId = Integer.parseInt(commandToken[1]); 
                if (commandToken.length == 3) {
                    commandResult = command.artical(userId).toString();
                } else {
                    newsId = Integer.parseInt(commandToken[2]); 
                    commandResult = command.artical(userId, newsId).toString();
                }
                data = commandResult.getBytes();
                return data;

        }
        return null;
    }
}
