/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.util.Date;

/**
 *
 * @author Sana
 */
public class News {

    private int newsId;
    private int groupId;
    private int userId;
    private String newsBody;
    private String newsHead;
    private Date newsDate;

    public News(int newsId, int groupId, int userId, String newsBody, String newsHead) {
        this.newsId = newsId;
        this.groupId = groupId;
        this.userId = userId;
        this.newsBody = newsBody;
        this.newsHead = newsHead;
        this.newsDate = new Date();
    }

    public int getNewsId() {
        return newsId;
    }

    public void setNewsId(int newsId) {
        this.newsId = newsId;
    }

    public int getGroupId() {
        return groupId;
    }

    public void setGroupId(int groupId) {
        this.groupId = groupId;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public String getNewsBody() {
        return newsBody;
    }

    public void setNewsBody(String newsBody) {
        this.newsBody = newsBody;
    }

    public String getNewsHead() {
        return newsHead;
    }

    public void setNewsHead(String newsHead) {
        this.newsHead = newsHead;
    }

    public Date getNewsDate() {
        return newsDate;
    }

    public void setNewsDate(Date newsDate) {
        this.newsDate = newsDate;
    }
    
    
}
