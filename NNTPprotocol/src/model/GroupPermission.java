/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

/**
 *
 * @author Sana
 */
public class GroupPermission {

    private int permissionId;
    private int groupId;
    private int userId;
    private boolean readPermission;
    private boolean writePermission;

    public GroupPermission(int permissionId, int groupId, int userId, boolean readPermission, boolean writePermission) {
        this.permissionId = permissionId;
        this.groupId = groupId;
        this.userId = userId;
        this.readPermission = readPermission;
        this.writePermission = writePermission;
    }

    public int getPermissionId() {
        return permissionId;
    }

    public void setPermissionId(int permissionId) {
        this.permissionId = permissionId;
    }

    public int getGroupId() {
        return groupId;
    }

    public void setGroupId(int groupId) {
        this.groupId = groupId;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public boolean isReadPermission() {
        return readPermission;
    }

    public void setReadPermission(boolean readPermission) {
        this.readPermission = readPermission;
    }

    public boolean isWritePermission() {
        return writePermission;
    }

    public void setWritePermission(boolean writePermission) {
        this.writePermission = writePermission;
    }
}
