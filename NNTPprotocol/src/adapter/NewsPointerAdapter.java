/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package adapter;

import static adapter.DatabaseAdapter.getConnection;
import static adapter.NewsAdapter.TABLE_NAME;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import model.News;
import model.NewsPointer;

/**
 *
 * @author Sana
 */
public class NewsPointerAdapter extends DatabaseAdapter {

    private static final String POINTER_ID = "POINTER_ID";
    private static final String NEWS_ID = "NEWS_ID";
    private static final String USER_ID = "USER_ID";
    public static final String TABLE_NAME = "NEWS_POINTER";

    public static void addNewsPointer(int newsId, int userId) {
        try {
            Connection conn = getConnection();
            Statement st = conn.createStatement();
            st.executeUpdate("INSERT INTO " + TABLE_NAME + " (" + NEWS_ID + ", "
                    + USER_ID + ") VALUES (" + newsId + ", " + userId + ")");

            st.close();

        } catch (Exception e) {
            System.out.println("Error - " + e.toString());
        }
    }

    public static void updatePointer(int pointerId, int newsId) {
        try {
            Connection conn = getConnection();
            Statement st = conn.createStatement();
            st.executeUpdate("UPDATE " + TABLE_NAME + " SET " + NEWS_ID + "=" + newsId
                    + " WHERE " + POINTER_ID + "=" + pointerId);
            st.close();

        } catch (Exception e) {
            System.out.println("Error - " + e.toString());
        }
    }

    public static NewsPointer getNewsPointer(int userId) {
        NewsPointer pointer = null;
        try {
            Connection conn = getConnection();
            Statement st = conn.createStatement();
            ResultSet rec = st.executeQuery("SELECT *  FROM " + TABLE_NAME
                    + " WHERE " + USER_ID + "=" + userId);
            rec.next();
            pointer = new NewsPointer(rec.getInt(POINTER_ID), rec.getInt(USER_ID), rec.getInt(NEWS_ID));
            st.close();
        } catch (Exception e) {
            System.out.println("Error - " + e.toString());
        }
        return pointer;
    }
}
