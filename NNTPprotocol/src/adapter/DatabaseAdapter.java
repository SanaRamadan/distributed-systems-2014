/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package adapter;

import java.sql.Connection;
import java.sql.DriverManager;

/**
 *
 * @author Sana
 */
public abstract class DatabaseAdapter {

    private static final String HOST = "jdbc:derby://localhost:1527/NetworkNews1";
    private static final String USER_NAME = "admin1";
    private static final String USER_PASSWORD = "admin1";

    protected static Connection getConnection() {
        try {
            Class.forName("org.apache.derby.jdbc.ClientDriver");
            return DriverManager.getConnection(HOST, USER_NAME, USER_PASSWORD);
        } catch (Exception e) {
            return null;
        }
    }
}
