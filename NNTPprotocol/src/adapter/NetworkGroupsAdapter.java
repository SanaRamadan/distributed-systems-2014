/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package adapter;

import static adapter.DatabaseAdapter.getConnection;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import model.NetworkGroup;

/**
 *
 * @author Sana
 */
public class NetworkGroupsAdapter extends DatabaseAdapter {

    private static final String GROUP_ID = "GROUP_ID";
    private static final String GROUP_NAME = "GROUP_NAME";
    public static final String TABLE_NAME = "NETWORK_GROUPS";

    public static void addGroup(int groupId, String grouName) {
        try {
            Connection conn = getConnection();
            Statement st = conn.createStatement();
            st.executeUpdate("INSERT INTO " + TABLE_NAME + " (" + GROUP_ID + "," + GROUP_NAME
                    + ") VALUES(" + groupId + ", '" + grouName + "')");
            st.close();
        } catch (Exception e) {
            System.out.println("Error - " + e.toString());
        }
    }

    public static List<NetworkGroup> getAllGroups() {
        List<NetworkGroup> groups = null;
        try {
            Connection conn = getConnection();
            Statement st = conn.createStatement();
            ResultSet rec = st.executeQuery("SELECT *  FROM " + TABLE_NAME + " ORDER BY " + GROUP_NAME);
            groups = new ArrayList();
            while (rec.next()) {
                groups.add(new NetworkGroup(rec.getInt(GROUP_ID),rec.getString(GROUP_NAME)));
            }
            st.close();
        } catch (Exception e) {
            System.out.println("Error - " + e.toString());
        }
        return groups;
    }
}
