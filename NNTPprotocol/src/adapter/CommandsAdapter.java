/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package adapter;

import static adapter.DatabaseAdapter.getConnection;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import model.Command;
import model.News;

/**
 *
 * @author Sana
 */
public class CommandsAdapter extends DatabaseAdapter {

    private static final String COMMAND_ID = "COMMAND_ID";
    private static final String COMMAND_SYNTAX = "COMMAND_SYNTAX";
    public static final String COMMAND_DESCRIPTION = "COMMAND_DESCRIPTION";
    public static final String TABLE_NAME = "COMMANDS";

    public static void addCommand(int commandId, String commandSyntax, String commandDescription) {
        try {
            Connection conn = getConnection();
            Statement st = conn.createStatement();
            st.executeUpdate("INSERT INTO " + TABLE_NAME + " ("
                    + COMMAND_ID + "," + COMMAND_SYNTAX + "," + COMMAND_DESCRIPTION
                    + ") VALUES(" + commandId + ", '" + commandSyntax + ", '" + commandDescription + "')");
            st.close();
        } catch (Exception e) {
            System.out.println("Error - " + e.toString());
        }
    }

 /*   public static List<Command> getAllCommand() {
        List<Command> commands = null;
        try {
            Connection conn = getConnection();
            Statement st = conn.createStatement();
            ResultSet rec = st.executeQuery("SELECT *  FROM " + TABLE_NAME);
         commands = new ArrayList();
            while (rec.next()) {
            commands.add( new Command(rec.getInt(COMMAND_ID),
                    rec.getString(COMMAND_SYNTAX), rec.getString(COMMAND_DESCRIPTION)));
            }   st.close();
        } catch (Exception e) {
            System.out.println("Error - " + e.toString());
        }
        return commands;
    }*/


} 
