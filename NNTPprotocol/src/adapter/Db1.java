/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package adapter;

import java.util.Date;
import manager.CommandARTICLE;
import manager.CommandGROUP;
import manager.CommandLIST;
import manager.CommandNEXT;
import manager.CommandPOST;
import manager.CommandPREVIOUS;

/**
 *
 * @author Sana
 */
public class Db1 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {

        NetworkUsersAdapter.addUser(1, "Sana", "sana@gmail.com", "sana123");

        NetworkGroupsAdapter.addGroup(1, "goverment");
        NetworkGroupsAdapter.addGroup(2, "weather");
        NetworkGroupsAdapter.addGroup(3, "art");
        NetworkGroupsAdapter.addGroup(4, "sport");
        NetworkGroupsAdapter.addGroup(5, "technology");

        GroupPermissionAdapter.addPermission(1, 1, 1, true, true);
        GroupPermissionAdapter.addPermission(2, 1, 2, true, false);
        GroupPermissionAdapter.addPermission(3, 1, 3, false, false);
        GroupPermissionAdapter.addPermission(4, 1, 4, true, true);
        GroupPermissionAdapter.addPermission(5, 1, 5, true, true);

        NewsAdapter.addNews(3, new Date(), "Twitter",
                "منع تويتر في تركيا", 1, 1);
        NewsAdapter.addNews(4, new Date(), "Android",
                "نظام تشغيل أندرويد للأجهزة القابلة للارتداء", 1, 5);
        NewsAdapter.addNews(5, new Date(), "Top Ten",
                "كرم سالم بالمرتبة الأولى", 1, 3);
        NewsAdapter.addNews(6, new Date(), "Rainy Coat",
                "هطول أمطار غزيرة في كندا في غير أوقاتها", 1, 2);
        NewsAdapter.addNews(7, new Date(), "temprature",
                "انخفاض درجة الحرارة عن معدلاتها في دول حوض المتوسط", 1, 2);
        NewsAdapter.addNews(8, new Date(), "cinema festival",
                "مهرجان السينما في دار الأوبرا", 1, 3);
        NewsAdapter.addNews(9, new Date(), "Spain sport",
                "تعادل فريق برشلونة مع ريال مدريد", 1, 4);
        NewsAdapter.addNews(10, new Date(), "Windows OS",
                "إصدار نسخة حديثة من نظام التشغيل windows8", 1, 5);
        NewsAdapter.addNews(11, new Date(), "اجتماع الجامعة العربية",
                "اجتماع جامعة الدول العربية", 1, 1);
        NewsAdapter.addNews(12, new Date(), "Egypt voting",
                "تحديد موعد الانتخابات الرئاسية في جمهورية مصر", 1, 1);
        NewsAdapter.addNews(13, new Date(), "Ali Ahmad gallery",
                "معرض الفنان علي أحمد", 1, 3);
        NewsAdapter.getNewsCount(5);
        System.out.print((String) new CommandLIST(1).execute() + "\n");

        System.out.print((String) new CommandGROUP(5).execute() + "\n");

        //    System.out.print((String) new CommandHELP().execute()); 
        System.out.print("\n*" + (String) new CommandNEXT(1).execute() + "\n");
        System.out.print("\n" + (String) new CommandPREVIOUS(1).execute() + "\n");

        System.out.print("\n" + (String) new CommandARTICLE(1).execute() + "\n");

        System.out.print("\n" + (String) new CommandPOST(1, 1).execute() + "\n");
        System.out.print("\n" + (String) new CommandPOST(1, 2).execute() + "\n");
        System.out.print("\n" + (String) new CommandPOST(1, 3).execute() + "\n");
        System.out.print("\n" + (String) new CommandPOST(1, 4).execute() + "\n");

    }
}
