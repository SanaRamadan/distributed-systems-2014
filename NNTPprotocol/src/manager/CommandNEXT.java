/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package manager;

import adapter.GroupPermissionAdapter;
import adapter.NewsAdapter;
import adapter.NewsPointerAdapter;
import model.Command;
import model.News;
import model.NewsPointer;

/**
 *
 * @author Sana
 */
public class CommandNEXT extends CommandModel {

    private News news;
    private int userId;

    public CommandNEXT(int userId) {
        this.userId = userId;
    }

    @Override
    public Object execute() {
        String result = "";
        int groupId;
        News nextNews;
        NewsPointer pointer = NewsPointerAdapter.getNewsPointer(userId);
        if (pointer == null) {
            groupId = GroupPermissionAdapter.getFirstReadPermission(userId).getGroupId();
            news = NewsAdapter.getFirstNews(groupId);
          result += "NewsHead: " +news.getNewsHead() + "*NewsBody: " + news.getNewsBody();
            nextNews = NewsAdapter.getNextNews(news.getNewsId());
            NewsPointerAdapter.addNewsPointer(nextNews.getNewsId(), userId); 
        } else {
            int newsId = pointer.getNewsId();
            nextNews = NewsAdapter.getNextNews(newsId);
            NewsPointerAdapter.updatePointer(pointer.getPointerId(), nextNews.getNewsId());
            result += "NewsHead: " +nextNews.getNewsHead() + "*NewsBody: " + nextNews.getNewsBody();
        }
        return result;
    }

    public static String commandInformation() {
        return new Command(3, "NEXT", "Return next news.").toString();
    }
}
