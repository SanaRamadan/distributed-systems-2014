/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package manager;

import adapter.GroupPermissionAdapter;
import adapter.NewsAdapter;
import java.util.Date;
import model.Command;
import model.News;

/**
 *
 * @author Sana
 */
public class CommandPOST extends CommandModel {

    private static final String WRITE_CODE = "340";
    private static final String READ_CODE = "440";
    private News news;
    private int userId;
    private int groupId;
    private String newsText;
    private boolean groupWritePermission;

    public CommandPOST(int userId, int groupId) {
        this.userId = userId;
        this.groupId = groupId;
          this.newsText = "";
    }

    public CommandPOST(int userId, int groupId, String newsText) {
        this.userId = userId;
        this.groupId = groupId;
        this.newsText = newsText;
    }

    @Override
    public Object execute() {
        String result = "";
        boolean writePermission = GroupPermissionAdapter.getGroupWritePermission(userId, groupId);
        if (newsText.compareTo("") == 0) {
            result += (writePermission ? WRITE_CODE : READ_CODE);
        } else {
            if (writePermission) {
                String[] news = newsText.split("*");
                NewsAdapter.addNews(100, new Date(), news[0], news[1], userId, groupId);
            }
        }
        return result;
    }

    public static String commandInformation() {
        return new Command(5, "POST", "To add new news by user.").toString();
    }
}
