/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package manager;

import adapter.GroupPermissionAdapter;
import adapter.NewsAdapter;
import adapter.NewsPointerAdapter;
import model.Command;
import model.News;
import model.NewsPointer;

/**
 *
 * @author Sana
 */
public class CommandARTICLE extends CommandModel {

    private News news;
    private int userId;
    private int newsId;

    public CommandARTICLE(int userId, int newsId) {
        this.userId = userId;
        this.newsId = newsId;
    }

    public CommandARTICLE(int userId) {
        this.userId = userId;
        this.newsId = -1;
    }

    @Override
    public Object execute() {
        String result = "";
        int groupId;
        News nextNews;
        if (newsId == -1) {
            groupId = GroupPermissionAdapter.getFirstReadPermission(userId).getGroupId();
            news = NewsAdapter.getFirstNews(groupId);
            result += "NewsHead: " +news.getNewsHead() + "*NewsBody: " + news.getNewsBody();
            nextNews = NewsAdapter.getNextNews(news.getNewsId());
            NewsPointerAdapter.addNewsPointer(nextNews.getNewsId(), userId);
        } else {
            news = NewsAdapter.getNews(newsId);

            if (news == null) {
                NewsPointer pointer = NewsPointerAdapter.getNewsPointer(userId);
                news = NewsAdapter.getNews(pointer.getNewsId());
               result += "NewsHead: " +news.getNewsHead() + "*NewsBody: " + news.getNewsBody();
            } else {
               result += "NewsHead: " +news.getNewsHead() + "*NewsBody: " + news.getNewsBody();
            }
        }
        return result;
    }

    public static String commandInformation() {
        return new Command(7, "ARTICLE", "Return news.").toString();
    }
}
