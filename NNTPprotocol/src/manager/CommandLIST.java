/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package manager;

import adapter.GroupPermissionAdapter;
import adapter.NetworkGroupsAdapter;
import adapter.NewsAdapter;
import java.util.List;
import model.Command;
import model.NetworkGroup;

/**
 *
 * @author Sana
 */
public class CommandLIST extends CommandModel {

    private int userId;
    private int groupId;
    private int newsCount;
    private int firstNews;
    private int lastNews;

    public CommandLIST(int userId) {
        this.userId = userId;
    }

    @Override
    public Object execute() {
        String result = "";
        List<NetworkGroup> groups = NetworkGroupsAdapter.getAllGroups();
        for (NetworkGroup group : groups) {
            int groupId = group.getGroupId();
            result += "GroupName " + group.getGroupName() + "*";
            if (NewsAdapter.getNewsCount(groupId) != 0) {
                result += "FirstNews " + NewsAdapter.getFirstNews(groupId).getNewsId() + "*";
                result += "LastNews" + NewsAdapter.getLastNews(groupId).getNewsId() + "*";
            }
            result += (GroupPermissionAdapter.getGroupWritePermission(userId, groupId) ? "Y" : "N");
            result += ",\n";
        }
        return result;
    }

    public static String commandInformation() {
        return new Command(0, "LIST", "Return group's news and number of them.").toString();
    }
}
