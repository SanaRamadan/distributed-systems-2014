/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package manager;

import model.Command;

/**
 *
 * @author Sana
 */
public class CommandHELP extends CommandModel {

    public CommandHELP() {
    }

    @Override
    public Object execute() {
        String result = "";
        
        result += CommandLIST.commandInformation() + ",\n";
        result += CommandGROUP.commandInformation() + ",\n";
        result += CommandHELP.commandInformation() + ",\n";
        result += CommandNEXT.commandInformation() + ",\n";
        result += CommandPREVIOUS.commandInformation() + ",\n";
        result += CommandPOST.commandInformation() + ",\n";
        result += CommandQUIT.commandInformation() + ",\n";
        result += CommandARTICLE.commandInformation() + "\n";

        return result;
    }

    public static String commandInformation() {
        return new Command(2, "HELP", "Return information about each command.").toString();
    }
}
