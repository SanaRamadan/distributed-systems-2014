/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package manager;

import adapter.GroupPermissionAdapter;
import adapter.NetworkGroupsAdapter;
import adapter.NewsAdapter;
import adapter.NewsPointerAdapter;
import java.util.List;
import model.Command;
import model.NetworkGroup;
import model.News;
import model.NewsPointer;

/**
 *
 * @author Sana
 */
public class CommandPREVIOUS extends CommandModel {

    private News news;
    private int userId;

    public CommandPREVIOUS(int userId) {
        this.userId = userId;
    }

    @Override
    public Object execute() {
        String result = "";
        int groupId;
        News previousNews;
        NewsPointer pointer = NewsPointerAdapter.getNewsPointer(userId);
        if (pointer == null) {
            groupId = GroupPermissionAdapter.getFirstReadPermission(userId).getGroupId();
            news = NewsAdapter.getFirstNews(groupId);
            result += news.getNewsHead() + "*" + news.getNewsBody();
            previousNews = NewsAdapter.getPreviousNews(news.getNewsId());
            NewsPointerAdapter.addNewsPointer(previousNews.getNewsId(), userId); 
        } else {
            int newsId = pointer.getNewsId();
            previousNews = NewsAdapter.getPreviousNews(newsId);
            NewsPointerAdapter.updatePointer(pointer.getPointerId(), previousNews.getNewsId());
            result += "NewsHead: " +previousNews.getNewsHead() + "*NewsBody: " + previousNews.getNewsBody();
        }
        return result;
    }

    public static String commandInformation() {
        return new Command(4, "PREVIOUS", "Return previous news.").toString();
    }
}
