/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package manager;

import adapter.NewsAdapter;
import model.Command;

/**
 *
 * @author Sana
 */
public class CommandGROUP extends CommandModel {

    private int groupId;
    private int newsCount;
    private int firstNews;
    private int lastNews;

    public CommandGROUP(int groupId) {
        this.groupId = groupId;
    }

    @Override
    public Object execute() {
        String result = "";

        result +="NewsCount: "+ NewsAdapter.getNewsCount(groupId) + "*";
        result += "FirstNews: "+NewsAdapter.getFirstNews(groupId).getNewsId() + "*";
        result += "LastNews: " + NewsAdapter.getLastNews(groupId).getNewsId();

        return result;
    }
 
    public static String commandInformation() {
        return new Command(1, "GROUP", "Return information about spacific group.").toString();
    }
}
