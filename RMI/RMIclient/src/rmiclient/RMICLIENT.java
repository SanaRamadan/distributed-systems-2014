/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rmiclient;

import command_model.ARTICLECommand;
import command_model.LISTCommand;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;
import manager.CommandsInterface;

/**
 *
 * @author Douaa-pc
 */
public class RMICLIENT {

    private static Scanner scanIn = new Scanner(System.in);

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        CommandsInterface obj = null;
        try {
            Registry regi = LocateRegistry.getRegistry("localhost", 1222);
            obj = (CommandsInterface) regi.lookup("//localhost:1222/CommandService");

            while (true) {

                System.out.println("\n"
                        + "Enter the command id\n");
                System.out.println(""
                        + "LIST command (1)\n"
                        + "GROUP command (2) \n"
                        + "HELP command (3) \n"
                        + "NEXT command (4) \n"
                        + "PREVIOUS command (5) \n"
                        + "POST command (6) \n"
                        + "POST command with News information(7) \n"
                        + "QUIT command (8) \n"
                        + "ARTICLE command (9) \n"
                        + "ARTICLE command with select spesific News(10) \n");

                int commandId = scanIn.nextInt();
                String commandResult = handleCommand(obj, commandId);
                if (commandResult.equals("")) {
                    break;
                }
                System.out.println(commandResult);
            }

            /*      for (LISTCommand.Group g : obj.list(1).getGroups()) {
             System.out.println(g.getGroupName());
             }

             System.out.println(obj.post(1, 1).isWritePermission());
             System.out.println(obj.post(1, 1 ,15, "المسابقة الوطنية للرسم", "فازت لوحةالفنان سالم العالم بالمرتبة الأولى ").isWritePermission());

             System.out.println(obj.next(1).getNewsHead());
             for(;;);*/
            /*  System.out.println(obj.previous(1).getNewsBody());

             System.out.println(obj.artical(1, 5).getNewsHead());

             System.out.println(obj.help().getCommands().get(3)); 
             System.out.println("GroupId: " + obj.group(2).getGroupId());
             System.out.println("NewsCount: " + obj.group(2).getNewsCount());
             System.out.println("FirstNews: " + obj.group(3).getFirstNews());
             System.out.println("LastNews: " + obj.group(1).getLastNews());*/
        } catch (Exception ex) {
            Logger.getLogger(RMICLIENT.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private static String handleCommand(CommandsInterface commandObject, int commandNum) throws RemoteException {

        String commandResult = "";
        String newsHead = "";
        String newsBody = "";
        int userId = 0;
        int groupId = 0;
        int newsId = 0;

        switch (commandNum) {
            case 1:
                System.out.println("Enter the user id");
                userId = scanIn.nextInt();
                commandResult = commandObject.list(userId).toString();
                return commandResult;

            case 2:
                System.out.println("Enter the group id");
                groupId = scanIn.nextInt();
                commandResult = commandObject.group(groupId).toString();
                return commandResult;

            case 3:
                commandResult = commandObject.help().toString();
                return commandResult;

            case 4:
                System.out.println("Enter the user id");
                userId = scanIn.nextInt();
                commandResult = commandObject.next(userId).toString();
                return commandResult;

            case 5:
                System.out.println("Enter the user id");
                userId = scanIn.nextInt();
                commandResult = commandObject.previous(userId).toString();
                return commandResult;

            case 6:
                System.out.println("Enter the user id");
                userId = scanIn.nextInt();
                System.out.println("Enter the group id");
                groupId = scanIn.nextInt();
                commandResult = commandObject.post(userId, groupId).toString();
                return commandResult;

            case 7:
                System.out.println("Enter the user id");
                userId = scanIn.nextInt();
                System.out.println("Enter the group id");
                groupId = scanIn.nextInt();
                System.out.println("Enter the news id");
                newsId = scanIn.nextInt();
                System.out.println("Enter the news head");
                scanIn.nextLine();
                String temp;
                while (!(temp = scanIn.nextLine()).equals("")) {
                    newsHead += temp + " ";
                }
                System.out.println("Enter the news body");
                while (!(temp = scanIn.next()).equals(".")) {
                    newsBody += temp + " ";
                }
                commandResult = commandObject.post(userId, groupId, newsId, newsHead, newsBody).toString();
                return commandResult;

            case 8:
                return commandResult;

            case 9:
                System.out.println("Enter the user id");
                userId = scanIn.nextInt();
                commandResult = commandObject.artical(userId).toString();
                return commandResult;

            case 10:
                System.out.println("Enter the user id");
                userId = scanIn.nextInt();
                System.out.println("Enter the news id");
                newsId = scanIn.nextInt();
                commandResult = commandObject.artical(userId, newsId).toString();
                return commandResult;

        }
        return null;
    }
}
