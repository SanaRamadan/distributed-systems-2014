/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package command_model;

import adapter.GroupPermissionAdapter;
import adapter.NewsAdapter;
import adapter.NewsPointerAdapter;
import java.io.Serializable;
import model.Command;
import model.GroupPermission;
import model.News;
import model.NewsPointer;

/**
 *
 * @author Douaa-pc
 */
public class ARTICLECommand implements Serializable {

    private static final long serialVersionUID = 1L;

    private int userId;
    private int newsId;
    private String newsHead;
    private String newsBody;

    public ARTICLECommand(int userId, int newsId) {
        this.userId = userId;
        this.newsId = newsId;
    }

    public ARTICLECommand(int userId) {
        this.userId = userId;
        this.newsId = -1;
    }

    public ARTICLECommand execute() {
        int groupId;
        News nextNews;
        News news;

        if (newsId == -1) {
            groupId = GroupPermissionAdapter.getFirstReadPermission(userId).getGroupId();
            news = NewsAdapter.getFirstNews(groupId);

            newsId = news.getNewsId();
            newsHead = news.getNewsHead();
            newsBody = news.getNewsBody();

            nextNews = NewsAdapter.getNextNews(news.getNewsId());
            NewsPointerAdapter.addNewsPointer(nextNews.getNewsId(), userId);
        } else {
            news = NewsAdapter.getNews(newsId);
            if (news == null) {
                NewsPointer pointer = NewsPointerAdapter.getNewsPointer(userId);
                news = NewsAdapter.getNews(pointer.getNewsId());
            }

            GroupPermission permission = GroupPermissionAdapter.getPermission(userId, news.getGroupId());
            if (permission.isReadPermission()) {
                newsHead = news.getNewsHead();
                newsBody = news.getNewsBody();
            } else {
                newsHead = "No Permission";
                newsBody = "";
            }
        }
        return this;
    }

    public static String commandInformation() {
        return new Command(7, "ARTICLE", "Return news.").toString();
    }

    @Override
    public String toString() {
        return "ARTICLECommand{" + "userId=" + userId + ", newsId=" + newsId + ", newsHead=" +  newsBody  + ", newsBody=" + newsHead + '}';
    }

    public String getNewsHead() {
        return newsHead;
    }

    public void setNewsHead(String newsHead) {
        this.newsHead = newsHead;
    }

    public String getNewsBody() {
        return newsBody;
    }

    public void setNewsBody(String newsBody) {
        this.newsBody = newsBody;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public int getNewsId() {
        return newsId;
    }

    public void setNewsId(int newsId) {
        this.newsId = newsId;
    }
}
