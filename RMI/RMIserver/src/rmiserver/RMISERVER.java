/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rmiserver;

import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;
import manager.Commands;
import manager.CommandsInterface;
import static manager.ConnectInterface.HOST;
import static manager.ConnectInterface.SERVICE_NAME;
import manager.ConnectInterface;

/**
 *
 * @author Douaa-pc
 */
public class RMISERVER {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        try {
            int dbNO = 1;
            int port = 1119;
            Scanner scanIn = new Scanner(System.in);
            
            System.out.println("Enter port number");
            port = scanIn.nextInt();
           
            System.out.println("Enter database number");
            dbNO = scanIn.nextInt();
            scanIn.close(); 

            CommandsInterface command = new Commands(dbNO);

            Registry reg = LocateRegistry.createRegistry(port);
            reg.bind("//" + HOST + ":" + port + "/" + SERVICE_NAME, command);

            Registry regi = LocateRegistry.getRegistry(HOST, 1222);
            ConnectInterface connector = (ConnectInterface) regi.lookup("//" + HOST + ":" + 1222 + "/" + "ConnectService");
            connector.Connect(port);

        } catch (Exception ex) {
            Logger.getLogger(RMISERVER.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

}
