/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.Date;

/**
 *
 * @author Sana
 */
public class News implements Serializable {

    private static final long serialVersionUID = 1320L;

    transient private int newsId;
    transient private int groupId;
    transient private int userId;
    transient private String newsBody;
    transient private String newsHead;

    public News(){  }
    
    public News(int newsId, int groupId, int userId, String newsBody, String newsHead) {
        this.newsId = newsId;
        this.groupId = groupId;
        this.userId = userId;
        this.newsBody = newsBody;
        this.newsHead = newsHead;
    }

    private void readObject(ObjectInputStream aInputStream) throws ClassNotFoundException, IOException {
        //always perform the default de-serialization first
        aInputStream.defaultReadObject();
        int g = aInputStream.readInt();
        int n = aInputStream.readInt();
        int u = aInputStream.readInt();
        String b = aInputStream.readUTF();
        String h = aInputStream.readUTF();
        this.newsId = n;
        this.groupId = g;
        this.userId = u;
        this.newsBody = b;
        this.newsHead = h;
    }

    private void writeObject(ObjectOutputStream aOutputStream) throws IOException {
        //perform the default serialization for all non-transient, non-static fields
        aOutputStream.defaultWriteObject();

        aOutputStream.writeInt(getGroupId());
        aOutputStream.writeInt(getNewsId());
        aOutputStream.writeInt(getUserId());
        aOutputStream.writeChars(getNewsBody());
        aOutputStream.writeChars(getNewsHead());
    }

    public int getNewsId() {
        return newsId;
    }

    public void setNewsId(int newsId) {
        this.newsId = newsId;
    }

    public int getGroupId() {
        return groupId;
    }

    public void setGroupId(int groupId) {
        this.groupId = groupId;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public String getNewsBody() {
        return newsBody;
    }

    public void setNewsBody(String newsBody) {
        this.newsBody = newsBody;
    }

    public String getNewsHead() {
        return newsHead;
    }

    public void setNewsHead(String newsHead) {
        this.newsHead = newsHead;
    }

}
