/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package manager;

import command_model.ARTICLECommand;
import command_model.LISTCommand;
import command_model.NEXTCommand;
import command_model.POSTCommand;
import command_model.PREVIOUSCommand;
import command_model.QUITCommand;
import command_model.GROUPCommand;
import command_model.HELPCommand;
import java.rmi.Remote;
import java.rmi.RemoteException; 

/**
 *
 * @author Douaa-pc
 */
public interface CommandsInterface extends Remote {

    public LISTCommand list(int userId) throws RemoteException;

    public GROUPCommand group(int groupId) throws RemoteException;

    public HELPCommand help() throws RemoteException;

    public NEXTCommand next(int userId) throws RemoteException;

    public PREVIOUSCommand previous(int userId) throws RemoteException;

    public POSTCommand post(int userId, int groupId) throws RemoteException;

    public POSTCommand post(int userId, int groupId, int newsId, String newsHead, String newsBody)
            throws RemoteException;

    public QUITCommand quit() throws RemoteException;

    public ARTICLECommand artical(int userId, int newsId) throws RemoteException;

    public ARTICLECommand artical(int userId) throws RemoteException;
}
