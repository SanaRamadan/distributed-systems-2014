/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package manager;

import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import command_model.ARTICLECommand;
import command_model.GROUPCommand;
import command_model.HELPCommand;
import command_model.LISTCommand;
import command_model.NEXTCommand;
import command_model.POSTCommand;
import command_model.PREVIOUSCommand;
import command_model.QUITCommand;

/**
 *
 * @author Douaa-pc
 */
public class Commands extends UnicastRemoteObject implements CommandsInterface {

    private  int dbNO;
    public Commands(int dbNO) throws RemoteException {
        this.dbNO=dbNO;
    }

    @Override
    public synchronized GROUPCommand group(int groupId) {
        try {
            return new GROUPCommand(groupId).execute();
        } catch (Exception e) {
            throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        }
    }

    @Override
    public synchronized LISTCommand list(int userId) throws RemoteException {
        try {
            return new LISTCommand(userId).execute();
        } catch (Exception e) {
            throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        }
    }

    @Override
    public synchronized HELPCommand help() throws RemoteException {
        try {
            return new HELPCommand().execute();
        } catch (Exception e) {
            throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        }
    }

    @Override
    public synchronized NEXTCommand next(int userId) throws RemoteException {
        try {
            return new NEXTCommand(userId).execute();
        } catch (Exception e) {
            throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        }
    }

    @Override
    public synchronized PREVIOUSCommand previous(int userId) throws RemoteException {
        try {
            return new PREVIOUSCommand(userId).execute();
        } catch (Exception e) {
            throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        }
    }

    @Override
    public synchronized POSTCommand post(int userId, int groupId) throws RemoteException {
        try {
            return new POSTCommand(userId, groupId).execute();
        } catch (Exception e) {
            throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        }
    }

    @Override
    public synchronized POSTCommand post(int userId, int groupId, int newsId, String newsHead, String newsBody)
            throws RemoteException {
        try {
            return new POSTCommand(userId, groupId).execute(newsId, newsHead, newsBody);
        } catch (Exception e) {
            throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        }
    }

    @Override
    public synchronized QUITCommand quit() throws RemoteException {
        try {
            return new QUITCommand().execute();
        } catch (Exception e) {
            throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        }
    }

    @Override
    public synchronized ARTICLECommand artical(int userId, int newsId) throws RemoteException {
        try {
            return new ARTICLECommand(userId,newsId).execute();
        } catch (Exception e) {
            throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        }
    }

    @Override
    public  synchronized ARTICLECommand artical(int userId) throws RemoteException {
        try {
            return new ARTICLECommand(userId).execute();
        } catch (Exception e) {
            throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        }
    }
}
