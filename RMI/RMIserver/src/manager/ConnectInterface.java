/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package manager;

import java.rmi.Remote;
import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.HashMap;

/**
 *
 * @author Sana
 */
public interface ConnectInterface extends Remote {

    public static String SERVICE_NAME = "service";
    public static String HOST = "localhost";
    public static ArrayList<Integer> PORT_NUMBERS = new ArrayList<Integer>();
    public ArrayList<CommandsInterface> commandsObject = new ArrayList<CommandsInterface>();
    
    public void Connect(int port) throws RemoteException;

}
