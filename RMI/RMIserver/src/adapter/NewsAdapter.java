/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package adapter;

import static adapter.DatabaseAdapter.getConnection;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import model.NetworkGroup;
import model.News;

/**
 *
 * @author Sana
 */
public class NewsAdapter extends DatabaseAdapter {

    private static final String NEWS_ID = "NEWS_ID";
    private static final String NEWS_DATE = "NEWS_DATE";
    private static final String NEWS_HEAD = "NEWS_HEAD";
    private static final String NEWS_BODY = "NEWS_BODY";
    private static final String USER_ID = "USER_ID";
    private static final String GROUP_ID = "GROUP_ID";
    public static final String TABLE_NAME = "NEWS";

    public static void addNews(int newsId, Date newsDate, String newsHead, String newsBody, int userId, int groupId) {
        try {
            Connection conn = getConnection();
            Statement st = conn.createStatement();
            st.executeUpdate("INSERT INTO " + TABLE_NAME + " (" + NEWS_ID + "," + NEWS_DATE + ","
                    + NEWS_HEAD + "," + NEWS_BODY + "," + USER_ID + "," + GROUP_ID
                    + ") VALUES(" + newsId + ", '3/25/2014', '" + newsHead + "', '" + newsBody + "' ,"
                    + userId + ", " + groupId + ")");

            st.close();
        } catch (Exception e) {
            System.out.println("Error - " + e.toString());
        }
    }

    public static List< News> getAllNews() {
        List< News> news = new ArrayList();
        try {
            Connection conn = getConnection();
            Statement st = conn.createStatement();
            ResultSet rec = st.executeQuery("SELECT *  FROM " + TABLE_NAME);
            news = new ArrayList();
            while (rec.next()) {
                news.add(new News(rec.getInt(NEWS_ID), rec.getInt(GROUP_ID), rec.getInt(USER_ID),
                        rec.getString(NEWS_HEAD), rec.getString(NEWS_BODY)));
            }
            st.close();
        } catch (Exception e) {
            System.out.println("Error - " + e.toString());
            return news;
        }
        return news;
    }

    public static News getNews(int newsId) {
        News news  = new News();
        try {
            Connection conn = getConnection();
            Statement st = conn.createStatement();
            ResultSet rec = st.executeQuery("SELECT *  FROM " + TABLE_NAME
                    + " WHERE " + NEWS_ID + "=" + newsId);
            rec.next();
            news = new News(rec.getInt(NEWS_ID), rec.getInt(GROUP_ID), rec.getInt(USER_ID),
                    rec.getString(NEWS_HEAD), rec.getString(NEWS_BODY));
            st.close();
        } catch (Exception e) {
            System.out.println("Error - " + e.toString());
             return news;
        }
        return news;
    }

    public static News getFirstNewsForUser(int userId) {
        News news  = new News();
        try {
            Connection conn = getConnection();
            Statement st = conn.createStatement();
            ResultSet rec = st.executeQuery("SELECT *  FROM " + TABLE_NAME
                    + " WHERE " + USER_ID + "=" + userId + " ORDER BY " + NEWS_ID);
            rec.next();
            news = new News(rec.getInt(NEWS_ID), rec.getInt(GROUP_ID), rec.getInt(USER_ID),
                    rec.getString(NEWS_HEAD), rec.getString(NEWS_BODY));
            st.close();
        } catch (Exception e) {
            System.out.println("Error - " + e.toString());
             return news;
        }
        return news;
    }

    public static News getFirstNews() {
        News news  = new News();
        try {
            Connection conn = getConnection();
            Statement st = conn.createStatement();
            ResultSet rec = st.executeQuery("SELECT *  FROM " + TABLE_NAME
                     + " ORDER BY " + NEWS_ID);
            rec.next();
            news = new News(rec.getInt(NEWS_ID), rec.getInt(GROUP_ID), rec.getInt(USER_ID),
                    rec.getString(NEWS_HEAD), rec.getString(NEWS_BODY));
            st.close();
        } catch (Exception e) {
            System.out.println("Error - " + e.toString());
             return news;
        }
        return news;
    }
      public static News getFirstNews(int groupId) {
        News news = new News();
        try {
            Connection conn = getConnection();
            Statement st = conn.createStatement();
            ResultSet rec = st.executeQuery("SELECT *  FROM " + TABLE_NAME
                    + " WHERE " + GROUP_ID + "=" + groupId + " ORDER BY " + NEWS_ID);
            rec.next();
            news = new News(rec.getInt(NEWS_ID), rec.getInt(GROUP_ID), rec.getInt(USER_ID),
                    rec.getString(NEWS_HEAD), rec.getString(NEWS_BODY));
            st.close();
        } catch (Exception e) {
            System.out.println("Error - " + e.toString());
            return news;
        }
        return news;
    }

    public static News getNextNews(int newsId) {
        News news  = new News();
        try {
            Connection conn = getConnection();
            Statement st = conn.createStatement();
            ResultSet rec = st.executeQuery("SELECT *  FROM " + TABLE_NAME
                    + " WHERE " + NEWS_ID + ">" + newsId + " ORDER BY " + NEWS_ID );
            rec.next();
            news = new News(rec.getInt(NEWS_ID), rec.getInt(GROUP_ID), rec.getInt(USER_ID),
                    rec.getString(NEWS_HEAD), rec.getString(NEWS_BODY));
            st.close();
        } catch (Exception e) {
            System.out.println("Error - " + e.toString());
             return news;
        }
        return news;
    }
    
     public static News getPreviousNews(int newsId) {
        News news  = new News();
        try {
            Connection conn = getConnection();
            Statement st = conn.createStatement();
            ResultSet rec = st.executeQuery("SELECT *  FROM " + TABLE_NAME
                    + " WHERE " + NEWS_ID + " < " + newsId + " ORDER BY " + NEWS_ID + " DESC ");
            rec.next();
            news = new News(rec.getInt(NEWS_ID), rec.getInt(GROUP_ID), rec.getInt(USER_ID),
                    rec.getString(NEWS_HEAD), rec.getString(NEWS_BODY));
            st.close();
        } catch (Exception e) {
            System.out.println("Error - " + e.toString());
             return news;
        }
        return news;
    }

    public static News getLastNews(int groupId) {
        News news  = new News();
        try {
            Connection conn = getConnection();
            Statement st = conn.createStatement();
            ResultSet rec = st.executeQuery("SELECT *  FROM " + TABLE_NAME
                    + " WHERE " + GROUP_ID + "=" + groupId + " ORDER BY " + NEWS_ID + " DESC");
            rec.next();
            news = new News(rec.getInt(NEWS_ID), rec.getInt(GROUP_ID), rec.getInt(USER_ID),
                    rec.getString(NEWS_HEAD), rec.getString(NEWS_BODY));
            st.close();
        } catch (Exception e) {
            System.out.println("Error - " + e.toString());
             return news;
        }
        return news;
    }
        public static News getLastNews() {
        News news  = new News();
        try {
            Connection conn = getConnection();
            Statement st = conn.createStatement();
            ResultSet rec = st.executeQuery("SELECT *  FROM " + TABLE_NAME
                   + " ORDER BY " + NEWS_ID + " DESC");
            rec.next();
            news = new News(rec.getInt(NEWS_ID), rec.getInt(GROUP_ID), rec.getInt(USER_ID),
                    rec.getString(NEWS_HEAD), rec.getString(NEWS_BODY));
            st.close();
        } catch (Exception e) {
            System.out.println("Error - " + e.toString());
             return news;
        }
        return news;
    }

    public static int getNewsCount(int groupId) {
        int newsCount = 0;
        try {
            Connection conn = getConnection();
            Statement st = conn.createStatement();
            ResultSet rec = st.executeQuery("SELECT COUNT(*)  FROM " + TABLE_NAME
                    + " WHERE " + GROUP_ID + "=" + groupId);
            rec.next();
            newsCount = rec.getInt(1);
            st.close();
        } catch (Exception e) {
            System.out.println("Error - " + e.toString());
             return newsCount;
        }
        return newsCount;
    }
}
