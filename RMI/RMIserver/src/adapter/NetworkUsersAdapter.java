/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package adapter;

import java.io.File;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;

/**
 *
 * @author Sana
 */
public class NetworkUsersAdapter extends DatabaseAdapter {

    private static final String USER_NAME = "USER_NAME";
    private static final String USER_ID = "USER_ID";
    private static final String EMAIL = "EMAIL";
    private static final String PASSWORD = "PASSWORD";
    public static final String TABLE_NAME = "NETWORK_USERS";

    public static void addUser(int userId, String userName, String userEmail, String userPassword) {
        try {
            Connection conn = getConnection();
            Statement st = conn.createStatement();
            st.executeUpdate("INSERT INTO " + TABLE_NAME + " (" + USER_ID + ","
                    + USER_NAME + "," + EMAIL + "," + PASSWORD
                    + ") VALUES(" + userId + ", '" + userName + "', '" + userEmail + "', '" + userPassword + "')");
            st.close();
        } catch (Exception e) {
            System.out.println("Error - " + e.toString());
        }
    }
    
}
