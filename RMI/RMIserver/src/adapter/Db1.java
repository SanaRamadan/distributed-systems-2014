/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package adapter;

import command_model.LISTCommand;
import command_model.LISTCommand.Group;
import command_model.NEXTCommand;
import java.util.Date;

/**
 *
 * @author Sana
 */
public class Db1 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
         
         NetworkUsersAdapter.addUser(1, "Ruaa", "ruaa@gmail.com", "sana123");

         NetworkGroupsAdapter.addGroup(1, "goverment");
         NetworkGroupsAdapter.addGroup(2, "weather");
         NetworkGroupsAdapter.addGroup(3, "art");
         NetworkGroupsAdapter.addGroup(4, "sport");
         NetworkGroupsAdapter.addGroup(5, "technology");

         GroupPermissionAdapter.addPermission(1, 1, 1, true, true);
         GroupPermissionAdapter.addPermission(2, 1, 2, true, false);
         GroupPermissionAdapter.addPermission(3, 1, 3, false, false);
         GroupPermissionAdapter.addPermission(4, 1, 4, true, true);
         GroupPermissionAdapter.addPermission(5, 1, 5, true, true);

         NewsAdapter.addNews(3, new Date(), "Twitter",
         "منع تويتر في تركيا", 1, 1);
         NewsAdapter.addNews(4, new Date(), "Android",
         "نظام تشغيل أندرويد للأجهزة القابلة للارتداء", 1, 5);
         NewsAdapter.addNews(5, new Date(), "Top Ten",
         "كرم سالم بالمرتبة الأولى", 1, 3);
         NewsAdapter.addNews(6, new Date(), "Rainy Coat",
         "هطول أمطار غزيرة في كندا في غير أوقاتها", 1, 2);
         NewsAdapter.addNews(7, new Date(), "temprature",
         "انخفاض درجة الحرارة عن معدلاتها في دول حوض المتوسط", 1, 2);
         NewsAdapter.addNews(8, new Date(), "cinema festival",
         "مهرجان السينما في دار الأوبرا", 1, 3);
         NewsAdapter.addNews(9, new Date(), "Spain sport",
         "تعادل فريق برشلونة مع ريال مدريد", 1, 4);
         NewsAdapter.addNews(10, new Date(), "Windows OS",
         "إصدار نسخة حديثة من نظام التشغيل windows8", 1, 5);
         NewsAdapter.addNews(11, new Date(), "اجتماع الجامعة العربية",
         "اجتماع جامعة الدول العربية", 1, 1);
         NewsAdapter.addNews(12, new Date(), "Egypt voting",
         "تحديد موعد الانتخابات الرئاسية في جمهورية مصر", 1, 1);
         NewsAdapter.addNews(13, new Date(), "Ali Ahmad gallery",
         "معرض الفنان علي أحمد", 1, 3);
        
           NewsAdapter.addNews(14, new Date(), "Syria",
         "انفجار ضخم يهز العاصمة دمشق", 1, 1);
         NewsAdapter.addNews(15, new Date(), "Microsoft",
         "إصدار نسخة جديدة من نظام التشغيل ويندوز", 1, 5);
         NewsAdapter.addNews(16, new Date(), "Basket Ball",
         "انعقاد دورة كرة السلة الحادية عشر في لبنان", 1, 3);
         NewsAdapter.addNews(17, new Date(), "Strome",
         "عاصفة رملية تضرب دول الخليج العربي ", 1, 2);
         NewsAdapter.addNews(18, new Date(), "Hightemprature",
         "درجات الحرارة أقل من معدلاتها بخمس درجات في سوريا", 1, 2);
         NewsAdapter.addNews(19, new Date(), "Arab Idol",
         "فوز الشاب السوري شادي بلقب عرب أيدول لهذا الموسم", 1, 3);
         NewsAdapter.addNews(20, new Date(), "Football",
         "كأس العالم لكرة القدم في تنافس على مكان الانقعاد", 1, 4);
         NewsAdapter.addNews(21, new Date(), "Nokia",
         "نوكيا في صدد إصدار هاتف ذكي بنظام أندرويد", 1, 5);
         NewsAdapter.getNewsCount(5);
        
     //    "قرار وزاري", "إصدار مرسوم الدورة التكميلية"
        
         

        for (Group g : new LISTCommand(1).execute().getGroups()) {
            System.out.println(g.getGroupName());
        }
     //   System.out.println(NewsAdapter.getNewsCount(5));
             System.out.print("\n*" + new NEXTCommand(1).execute().getNewsHead() + "\n");
        //   System.out.print("\n*" + new NEXTCommand(1).execute().getNewsId() + "\n");
    /*    System.out.println(new ARTICLECommand(1, 1).execute().getNewsHead());
         System.out.println(new POSTCommand(1, 2).execute().isWritePermission());
         System.out.println(new POSTCommand(1, 2).execute(15, "موعد امتحانات الفصل الثاني", " تقريب موعد الامتحانات إلى15/5").isWritePermission());
         */
        //    System.out.print("\n*" + new PREVIOUSCommand(1).execute().getNewsBody() + "\n");
    }
}
