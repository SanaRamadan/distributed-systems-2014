/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rmimasterserver;

import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry; 
import java.util.logging.Level;
import java.util.logging.Logger;
import manager.Commands;
import manager.CommandsInterface;
import manager.ConnectInterface;
import static manager.ConnectInterface.HOST; 
import manager.Connector;

/**
 *
 * @author Sana
 */
public class RMIMASTERSERVER {

    public static void main(String[] args) {

        ConnectInterface connector = null;
        CommandsInterface command = null;
        try {
            connector = new Connector();
            command = new Commands(connector);

            Registry reg = LocateRegistry.createRegistry(1222);
            reg.bind("//" + HOST + ":" + 1222 + "/" + "ConnectService", connector);
            reg.bind("//" + HOST + ":" + 1222 + "/" + "CommandService", command);

        } catch (Exception ex) {
            Logger.getLogger(RMIMASTERSERVER.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

}
