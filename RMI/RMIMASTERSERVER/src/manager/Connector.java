/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package manager;

import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;
import java.util.ArrayList;

/**
 *
 * @author Douaa-pc
 */
public class Connector extends UnicastRemoteObject implements ConnectInterface {


    public Connector() throws RemoteException {
        CommandsInterface command = null;
       int port;
        try {
            for (int i = 0; i < PORT_NUMBERS.size(); i++) {
                port = PORT_NUMBERS.get(i);
                Registry regi = LocateRegistry.getRegistry(HOST, port);
                command = (CommandsInterface) regi.lookup("//" + HOST + ":" + port + "/" + SERVICE_NAME);
                commandsObject.add(command);
            }
        } catch (Exception e) {
            throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        }
    }

    @Override
    public void Connect(int port) throws RemoteException {
        try {
            PORT_NUMBERS.add(port);
            Registry regi = LocateRegistry.getRegistry(HOST, port);
            CommandsInterface command = (CommandsInterface) regi.lookup("//" + HOST + ":" + port + "/" + SERVICE_NAME);
            commandsObject.add(command);

        } catch (Exception e) {
            throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        }
    }

}
