/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package command_model;

import java.io.Serializable;
import java.rmi.RemoteException;
import java.util.ArrayList;
import manager.CommandsInterface;

/**
 *
 * @author Douaa-pc
 */
public class NEXTCommand implements Serializable {

    private static final long serialVersionUID = 1L;

    private int userId;
    private int newsId;
    int groupId;
    private String newsBody;
    private String newsHead;

    public NEXTCommand(int userId) {
        this.userId = userId;
    }

    public NEXTCommand execute(CommandsInterface commandsObject) throws RemoteException {
        groupId = commandsObject.next(userId).groupId;
        newsId = commandsObject.next(userId).newsId;
        newsBody = commandsObject.next(userId).newsBody;
        newsHead = commandsObject.next(userId).newsHead;
        return this;
    }

    public int getNewsId() {
        return newsId;
    }

    public void setNewsId(int newsId) {
        this.newsId = newsId;
    }

    public int getGroupId() {
        return groupId;
    }

    public void setGroupId(int groupId) {
        this.groupId = groupId;
    }

    public String getNewsBody() {
        return newsBody;
    }

    public void setNewsBody(String newsBody) {
        this.newsBody = newsBody;
    }

    public String getNewsHead() {
        return newsHead;
    }

    public void setNewsHead(String newsHead) {
        this.newsHead = newsHead;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

}
