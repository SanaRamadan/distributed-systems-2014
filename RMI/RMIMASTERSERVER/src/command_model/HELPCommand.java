/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package command_model;

import java.io.Serializable;
import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.List; 
import java.util.logging.Level;
import java.util.logging.Logger;
import manager.CommandsInterface;

/**
 *
 * @author Douaa-pc
 */
public class HELPCommand implements Serializable {

    private static final long serialVersionUID = 1L;

    private List<String> commands;

    public HELPCommand() {
        commands =new ArrayList<String>();
    }

    public HELPCommand execute(CommandsInterface commandsObject) { 
        try {
            return commandsObject.help();
        } catch (RemoteException ex) {
            Logger.getLogger(HELPCommand.class.getName()).log(Level.SEVERE, null, ex);
            return this;
        }
    } 

    public List<String> getCommands() {
        return commands;
    }

    public void setCommands(List<String> commands) {
        this.commands = commands;
    }
    
}
